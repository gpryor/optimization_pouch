% STEP B
% in: s{n}.psi s{n}.mu      out: s{n}.q
psi = s{n}.psi; mu = s{n}.mu;

alpha = c_P_f1 * f1_D1_c * psi + mu{1} / R;
beta_x = f2_D2_c * psi + f2_P_c * mu{2} / R;
beta_y = f3_D3_c * psi + f3_P_c * mu{3} / R;



% new version on cell centers
% solve for updated a and b
cvx_begin
  variables a(n_c) b_x(n_c) b_y(n_c)
  minimize sum((a - alpha).^2) + sum((b_x - beta_x).^2) + sum((b_y - beta_y).^2)
subject to
  a + (b_x .^ 2 + b_y .^ 2) / 2 <= 0;
cvx_end
q{1} = a;
q{2} = b_x;
q{3} = b_y;



% % old version on cell faces
% if 0
% % solve for updated a and b
% cvx_begin
%   variables a(n_c) b_x(n_f2) b_y(n_f3)
%   minimize sum((a - alpha).^2) + sum((b_x - beta_x).^2) + sum((b_y - beta_y).^2)
% subject to
%   a + (c_P_f2 * (b_x .^ 2) + c_P_f3 * (b_y .^ 2)) / 2 <= 0;
% cvx_end
% q{1} = a;
% q{2} = b_x;
% q{3} = b_y;
% end



% sum((a - alpha).^2) + sum((b_x - beta_x).^2) + sum((b_y - beta_y).^2)
if ~all(a + (b_x .^ 2 + b_y .^ 2) / 2 < 1e-6)    % fudge factor
  max(a + (b_x .^ 2 + b_y .^ 2) / 2)
  disp('bad bad bad bad thing!'); pause
  return
end

% gvolume(reshape(alpha, m_c));
% plot(alpha);

% plot(q{1}(:))
% plot(alpha); hold on; plot(a(:), 'r'); hold off;
% plot(b_y); hold on; plot(beta_y(:), 'r'); hold off;
