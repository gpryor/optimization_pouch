%%%%% algorithm is below
clear all;
setup;
cvx_quiet false

% STATE INFORMATION IS;    psi, q, mu
% start with valid state (at least for mu and q)
% init_mu;     % checks out fully
% init_q;      % appears to oscillate too much

[rho m_x m_y] = deal(rand(n_c,1 ));
[a b_x b_y] = deal(rand(n_c,1 ));

% solver
R = 0.5;
% niter = 100;
while (true)

  % step 1; min L wrt psi
  cvx_begin
    variables psi(n_c)
    minimize cvx_test(m, psi, a, b_x, b_y, rho, m_x, m_y, rho0, rhoT, R)
  cvx_end

  % step 2; min L wrt q
  % cvx_begin
  %   variables b_x(n_c) b_y(n_c) a(n_c)
  %   minimize cvx_test(m, psi, a, b_x, b_y, rho, m_x, m_y, rho0, rhoT, R)
  % subject to
  %   a + (b_x.^2 + b_y.^2) ./ 2 <= 0;
  % cvx_end

  kappa = -rho;
  b_x = -m_x ./ kappa;
  b_y = -m_y ./ kappa;
  a = -(b_x.^2 + b_y.^2) ./ 2;

  % step 3 (gradient descent)
  rho = rho + R * (c_P_f1 * f1_D1_c * psi -   a);
  m_x = m_x + R * (c_P_f2 * f2_D2_c * psi - b_x);
  m_y = m_y + R * (c_P_f3 * f3_D3_c * psi - b_y);

  % status
  plot(rho); drawnow;
  c7  = norm(c_P_f1 * f1_D1_c * rho + c_D2_f2 * f2_P_c * m_x + c_D3_f3 * f3_P_c * m_y);
  c8a = norm([rho(front) - rho0(:); zeros(n_c - n_front, 1)]);
  c8b = norm([zeros(n_c - n_back, 1); rho(back) - rhoT(:)]);
  fprintf('finished iter %d - - %f, %f %f\n', i, c7, c8a, c8b);
end
