% solver capable of non-cube shaped grids - rho on f1, m_x, m_y, on f2, f3
function attempt5_solver(problem)

  % unpack problem, grid metadata, stencils
  % problem
  m = problem.m; rho0 = problem.rho0; rhoT = problem.rhoT;
  % grid metadata
  s = gen_metadata(m, 'struct');
  m = s.m; m_c = s.m_c; m_n = s.m_n;
  m_f1 = s.m_f1; m_f2 = s.m_f2; m_f3 = s.m_f3;
  n_c  = s.n_c; n_n  = s.n_n; n_f1 = s.n_f1; n_f2 = s.n_f2; n_f3 = s.n_f3;
  slice_len = m_f3(1) * m_f3(2);
  front  = (1:slice_len);                    n_front = numel(front);
  back   = (1:slice_len) + n_f3 - slice_len; n_back  = numel(back);
  % stencils
  s = gen_stencils(m, 'struct');
  [v, d] = max(m);
  s1 = v / m(1); s2 = v / m(2); s3 = v / m(3);
  f1_D1_c = s1 * s.f1_D1_c; f2_D2_c = s2 * s.f2_D2_c; f3_D3_c = s3 * s.f3_D3_c;
  c_D1_f1 = s1 * s.c_D1_f1; c_D2_f2 = s2 * s.c_D2_f2; c_D3_f3 = s3 * s.c_D3_f3;
  c_P_f1  = s.c_P_f1;  c_P_f2  = s.c_P_f2;  c_P_f3  = s.c_P_f3;
  f1_P_c  = s.f1_P_c;  f2_P_c  = s.f2_P_c;  f3_P_c  = s.f3_P_c;




  % random starting point
  rho = deal(ones(n_f3, 1));
  m_x = deal(zeros(n_f2, 1));
  m_y = deal(zeros(n_f1, 1));
  rho(front) = rho0; rho(back) = rhoT;
  % % smooth starting point
  % [rho, m_x, m_y] = attempt3_x0(rho0, rhoT, m);
  % % identity m_x, m_y starting point
  % rho = repmat(rho0, [1 1 m_f3(3)]); rho = rho(:);
  % m_x = deal(zeros(n_f2, 1)); m_y = deal(zeros(n_f1, 1));
  x = [rho; m_x; m_y];


  % good q based on starting point
  [A, B_x, B_y] = linear_effort(get_rho(x), get_m_x(x), get_m_y(x));
  % true_eff = 1/2 * (c_P_f3 * (get_rho(x).^-1)) .* ...
  %     (c_P_f2 * (get_m_x(x).^2) + c_P_f1 * (get_m_y(x).^2));
  % approx_eff = A * c_P_f3 * rho + B_x * c_P_f2 * m_x + B_y * c_P_f1 * m_y;
  % plot(true_eff); hold on; plot(approx_eff, 'r'); hold off;
  % legend('true', 'approx');
  % fcheck(true_eff, approx_eff);

  % augmented lagrangian solver
  % estimates for langrange multipliers, penalities
  u_1 =  ones(n_c, 1); [u_2, u_3] = deal( ones(n_f3, 1));
  l_1 = zeros(n_c, 1); [l_2, l_3] = deal(zeros(n_f3, 1));

  % % possible unit testing at start point
  % l_1 = rand(n_c, 1); [l_2 l_3] = deal(rand(n_f3, 1)); unit_test; return

  % convergence tolerances
  tau_inner = 1e-3; tau_outer = 1e-6;
  while (true)

    L0 = obj(get_rho(x), get_m_x(x), get_m_y(x));
    iter = 0;
    while (true)
      iter = iter + 1;

      % % blur things a bit
      % tmp = reshape(get_m_y(x), m_f1);
      % tmp = convn(tmp, ones(3,3,3)/ 27, 'same');
      % x(n_f3 + n_f2 + 1:end) = tmp(:);

      % q; effort term as sup_q(convex function)
      [A, B_x, B_y] = linear_effort(get_rho(x), get_m_x(x), get_m_y(x));

      % make sure that approximation matches effort
      % true_eff = 1/2 * (c_P_f3 * (get_rho(x).^-1)) .* ...
      %     (c_P_f2 * (get_m_x(x).^2) + c_P_f1 * (get_m_y(x).^2));
      % true_eff = 1/2 * ((c_P_f3 * get_rho(x)).^-1) .* ...
      %     ((c_P_f2 * get_m_x(x)).^2 + (c_P_f1 * get_m_y(x)).^2);
      % approx_eff = A * c_P_f3 * get_rho(x) + ...
      %     B_x * c_P_f2 * get_m_x(x) + ...
      %     B_y * c_P_f1 * get_m_y(x);
      % plot(true_eff); hold on; plot(approx_eff, 'r'); hold off; drawnow
      % fcheck(true_eff, approx_eff);

      % armijo rule line search parameters (constant settings)
      beta = 0.5^4; alpha = 1e-4;
      f = @(x) obj(get_rho(x), get_m_x(x), get_m_y(x));

      old_f = f(x);

      % line search
      L = obj(get_rho(x), get_m_x(x), get_m_y(x));
      L_x = obj_x(get_rho(x), get_m_x(x), get_m_y(x));
      m = 1;
      while m <= 20
        lambda = beta ^ m;
        accept = f(x - lambda * L_x) - L < -alpha * lambda * sum(L_x.^2);
        if accept, break; end
        m = m + 1;
      end

      % stop if could not make progress
      if m == 21, disp('inner loop could not go anywhere'); break; end

      % else, accept
      x = x - lambda * L_x;

      % stop on convergence (inner loop)
      if (norm(lambda * L_x) < tau_inner), disp('inner loop met tolerance'); break; end

      % status
      if ~mod(iter, 1), status; end
    end

    % stop on convergence (inner loop)
    % don't really know what to do here
    disp('inner loop converged');
    % pause

    % adjust l     used to be (l = l - 1/phi * constraint(x));
    l_1 = l_1 - (1 ./ u_1) .* c7 (get_rho(x), get_m_x(x), get_m_y(x));
    l_2 = l_2 - (1 ./ u_2) .* c8a(get_rho(x), get_m_x(x), get_m_y(x));
    l_3 = l_3 - (1 ./ u_3) .* c8b(get_rho(x), get_m_x(x), get_m_y(x));

    % adjust u
    u_1 = u_1 * 0.3;
    u_2 = u_2 * 0.3;
    u_3 = u_3 * 0.3;

    % only possibly break if converged to constraints
    c7_at_x  = c7 (get_rho(x), get_m_x(x), get_m_y(x));
    c8a_at_x = c8a(get_rho(x), get_m_x(x), get_m_y(x));
    c8b_at_x = c8b(get_rho(x), get_m_x(x), get_m_y(x));
    if norm(c7_at_x) < 1e-5 && norm(c8a_at_x) < 1e-5 && norm(c8b_at_x) < 1e-5
      % break on convergence
      L_ = obj(get_rho(x), get_m_x(x), get_m_y(x));
      if (norm(L_) / norm(L0) < tau_outer), break; end
    end

    % break on no progress
    if (m == 21), fprintf('no progress\n'); break; end
  end
  disp('outer loop converged');
  status

  


  function status
    L = obj(get_rho(x), get_m_x(x), get_m_y(x));
    true_effort = sum((c_P_f2 * get_m_x(x).^2 + c_P_f1 * get_m_y(x).^2) ./ (2 * c_P_f3 * get_rho(x)));
    c7_at_x = c7 (get_rho(x), get_m_x(x), get_m_y(x));
    c8a_at_x = c8a(get_rho(x), get_m_x(x), get_m_y(x));
    c8b_at_x = c8b(get_rho(x), get_m_x(x), get_m_y(x));
    fprintf('L: %f  effort: %f   c7: %f  c8a: %f  c8b: %f    m: %d\n', ...
            L, true_effort, norm(c7_at_x), norm(c8a_at_x), norm(c8b_at_x), m);

    BLAH = get_rho(x);
    BLAH = reshape(BLAH, [m_f3(1) m_f3(2) m_f3(3)]);
    % BLAH = penalty_x(get_rho(x), get_m_x(x), get_m_y(x));
    % BLAH = reshape(get_rho(BLAH), [m_f3(1) m_f3(2) m_f3(3)]);
    % BLAH = get_m_y(x);
    % BLAH = reshape(BLAH, [m_f1(1) m_f1(2) m_f1(3)]);
    gvolume(abs(BLAH));
    % mn = min(BLAH(:)); mx = max(BLAH(:));
    % gplot((BLAH - mn) / (mx - mn))

    % plot(get_rho(x)); drawnow;

    % BLAH = get_rho(x);
    % BLAH = reshape(BLAH, [m_f3(1) m_f3(2) m_f3(3)]);
    % j = 1;
    % for i=linspace(1, m_f3(3), 5)
    %   subplot(5,2,j); j = j + 1;
    %   imagesc(BLAH(:,:,i)); 
    % end
    % drawnow;
    % plot(get_rho(x)); drawnow;
  end



  % obj (augmented lagrangian, gradient for OMT)
  function L = obj(rho, m_x, m_y)
    L = 0 * effort(rho, m_x, m_y) - lagrange(rho, m_x, m_y) + penalty(rho, m_x, m_y);
  end
  function L = obj_x(rho, m_x, m_y)
    L = 0 * effort_x(rho, m_x, m_y) - lagrange_x(rho, m_x, m_y) + penalty_x(rho, m_x, m_y);
  end


  % effort
  % function [A, B_x, B_y] = linear_effort(rho, m_x, m_y)
  %   rho_inv = spdiags((c_P_f3 * rho).^-1, 0, n_c, n_c);
  %   norm_inv = spdiags((c_P_f2 * m_x).^2 + (c_P_f1 * m_y).^2, 0, n_c, n_c);
  %   A = -1/2 * rho_inv * rho_inv * norm_inv;
  %   B_x = rho_inv * spdiags(c_P_f2 * m_x, 0, n_c, n_c);
  %   B_y = rho_inv * spdiags(c_P_f1 * m_y, 0, n_c, n_c);
  % end
  % function L = effort(rho, m_x, m_y)
  %   L = sum(A * c_P_f3 * rho + B_x * c_P_f2 * m_x + B_y * c_P_f1 * m_y);
  % end
  % function L = effort_rho(rho, m_x, m_y), L = sum(A * c_P_f3)'; end
  % function L = effort_m_x(rho, m_x, m_y), L = sum(B_x * c_P_f2)'; end
  % function L = effort_m_y(rho, m_x, m_y), L = sum(B_y * c_P_f1)'; end

  function [A, B_x, B_y] = linear_effort(rho, m_x, m_y)
    rho_inv = spdiags(c_P_f3 * (rho.^-1), 0, n_c, n_c);
    A = -1/2 * rho_inv * ...
        spdiags(c_P_f2 * m_x.^2 + c_P_f1 * m_y.^2, 0, n_c, n_c) * ...
        c_P_f3 * spdiags(rho.^-1, 0, n_f3, n_f3);
    B_x = rho_inv * c_P_f2 * spdiags(m_x, 0, n_f2, n_f2);
    B_y = rho_inv * c_P_f1 * spdiags(m_y, 0, n_f1, n_f1);
  end
  function L = effort(rho, m_x, m_y)
    L = sum(A * rho + B_x * m_x + B_y * m_y);
  end
  function L = effort_rho(rho, m_x, m_y), L = sum(A)'; end
  function L = effort_m_x(rho, m_x, m_y), L = sum(B_x)'; end
  function L = effort_m_y(rho, m_x, m_y), L = sum(B_y)'; end
  function L = effort_x  (rho, m_x, m_y)
    tmp = map(@(fn) fn(rho, m_x, m_y), {@effort_rho, @effort_m_x, @effort_m_y});
    L = cat(1, tmp{:});
  end


  % lagrange
  function L = lagrange(rho, m_x, m_y)
    L = dot(l_1, c7 (rho, m_x, m_y)) + ...
        dot(l_2, c8a(rho, m_x, m_y)) + ...
        dot(l_3, c8b(rho, m_x, m_y));
  end
  function L = lagrange_rho(rho, m_x, m_y)
    L = c7_rho(rho, m_x, m_y)' * l_1 + ...
        c8a_rho(rho, m_x, m_y)' * l_2 + ...
        c8b_rho(rho, m_x, m_y)' * l_3;
  end
  function L = lagrange_m_x(rho, m_x, m_y)
    L =  c7_m_x(rho, m_x, m_y)' * l_1 + ...
        c8a_m_x(rho, m_x, m_y)' * l_2 + ...
        c8b_m_x(rho, m_x, m_y)' * l_3;
  end
  function L = lagrange_m_y(rho, m_x, m_y)
    L =  c7_m_y(rho, m_x, m_y)' * l_1 + ...
        c8a_m_y(rho, m_x, m_y)' * l_2 + ...
        c8b_m_y(rho, m_x, m_y)' * l_3;
  end
  function L = lagrange_x  (rho, m_x, m_y)
    tmp = map(@(fn) fn(rho, m_x, m_y), {@lagrange_rho, @lagrange_m_x, @lagrange_m_y});
    L = cat(1, tmp{:});
  end


  % penalty
  function L = penalty(rho, m_x, m_y)
    L = dot(1 ./ (2 * u_1),  c7(rho, m_x, m_y) .^ 2) + ...
        dot(1 ./ (2 * u_2), c8a(rho, m_x, m_y) .^ 2) + ...
        dot(1 ./ (2 * u_3), c8b(rho, m_x, m_y) .^ 2);
  end
  function L = penalty_rho(rho, m_x, m_y)
    L =  c7_rho(rho, m_x, m_y)' * ( c7(rho, m_x, m_y) .* (1 ./ u_1)) + ...
        c8a_rho(rho, m_x, m_y)' * (c8a(rho, m_x, m_y) .* (1 ./ u_2)) + ...
        c8b_rho(rho, m_x, m_y)' * (c8b(rho, m_x, m_y) .* (1 ./ u_3));
  end
  function L = penalty_m_x(rho, m_x, m_y)
    L =  c7_m_x(rho, m_x, m_y)' * ( c7(rho, m_x, m_y) .* (1 ./ u_1)) + ...
        c8a_m_x(rho, m_x, m_y)' * (c8a(rho, m_x, m_y) .* (1 ./ u_2)) + ...
        c8b_m_x(rho, m_x, m_y)' * (c8b(rho, m_x, m_y) .* (1 ./ u_3));
  end
  function L = penalty_m_y(rho, m_x, m_y)
    L =  c7_m_y(rho, m_x, m_y)' * ( c7(rho, m_x, m_y) .* (1 ./ u_1)) + ...
        c8a_m_y(rho, m_x, m_y)' * (c8a(rho, m_x, m_y) .* (1 ./ u_2)) + ...
        c8b_m_y(rho, m_x, m_y)' * (c8b(rho, m_x, m_y) .* (1 ./ u_3));
  end
  function L = penalty_x  (rho, m_x, m_y)
    tmp = map(@(fn) fn(rho, m_x, m_y), {@penalty_rho, @penalty_m_x, @penalty_m_y});
    L = cat(1, tmp{:});
  end


  % c7
  function L = c7(rho, m_x, m_y)
    L = c_D3_f3 * rho + c_D2_f2 * m_x + c_D1_f1 * m_y;
  end
  function L = c7_rho(rho, m_x, m_y), L = c_D3_f3; end
  function L = c7_m_x(rho, m_x, m_y), L = c_D2_f2; end
  function L = c7_m_y(rho, m_x, m_y), L = c_D1_f1; end
  function L = c7_x  (rho, m_x, m_y)
    tmp = map(@(fn) fn(rho, m_x, m_y), {@c7_rho, @c7_m_x, @c7_m_y});
    L = cat(2, tmp{:});
  end


  % c8a
  function L = c8a(rho, m_x, m_y)
    L = [rho(front) - rho0(:); zeros(n_f3 - n_front, 1)];
  end
  function L = c8a_rho(rho, m_x, m_y)
    L = speye(n_front);
    L(n_f3, numel(rho)) = 0;
  end 
  function L = c8a_m_x(rho, m_x, m_y), L = sparse(n_f3, numel(m_x)); end
  function L = c8a_m_y(rho, m_x, m_y), L = sparse(n_f3, numel(m_y)); end
  function L = c8a_x  (rho, m_x, m_y)
    tmp = map(@(fn) fn(rho, m_x, m_y), {@c8a_rho, @c8a_m_x, @c8a_m_y});
    L = cat(2, tmp{:});
  end


  % c8b
  function L = c8b(rho, m_x, m_y)
    L = [zeros(n_f3 - n_back, 1); rho(back) - rhoT(:)];
  end
  function L = c8b_rho(rho, m_x, m_y)
    d = [zeros(n_f3 - n_back, 1); ones(n_back, 1)];
    L = spdiags(d, 0, n_f3, numel(rho));
  end
  function L = c8b_m_x(rho, m_x, m_y), L = sparse(n_f3, numel(m_x)); end
  function L = c8b_m_y(rho, m_x, m_y), L = sparse(n_f3, numel(m_y)); end
  function L = c8b_x  (rho, m_x, m_y)
    tmp = map(@(fn) fn(rho, m_x, m_y), {@c8b_rho, @c8b_m_x, @c8b_m_y});
    L = cat(2, tmp{:});
  end




  % utility
  function rho = get_rho(x), rho = x(              1:n_f3       ); end
  function m_x = get_m_x(x), m_x = x(n_f3        + 1:n_f3 + n_f2); end
  function m_y = get_m_y(x), m_y = x(n_f3 + n_f2 + 1:        end); end
  function varargout = map(fn, varargin)
    varargout = cell(1,max(1,nargout));
    [varargout{:}] = cellfun(fn, varargin{:}, 'Un', 0);
  end


  % unit tests
  function unit_test
    % % c7
    % fprintf('c7_rho\n');
    % test_grad(rand(numel(rho), 1), ...
    %           @(y) c7    (y, get_m_x(x), get_m_y(x)), ...
    %           @(y) c7_rho(y, get_m_x(x), get_m_y(x)));
    % fprintf('c7_m_x\n');
    % test_grad(rand(numel(m_x), 1), ...
    %           @(y) c7    (get_rho(x), y, get_m_y(x)), ...
    %           @(y) c7_m_x(get_rho(x), y, get_m_y(x)));
    % fprintf('c7_m_y\n');
    % test_grad(rand(numel(m_y), 1), ...
    %           @(y) c7    (get_rho(x), get_m_x(x), y), ...
    %           @(y) c7_m_y(get_rho(x), get_m_x(x), y));
    % fprintf('c7_x\n');
    % test_grad(x, ...
    %           @(y) c7    (get_rho(y), get_m_x(y), get_m_y(y)), ...
    %           @(y) c7_x  (get_rho(y), get_m_x(y), get_m_y(y)));
    % % c8a
    % fprintf('c8a_rho\n');
    % test_grad(rand(numel(rho), 1), ...
    %           @(y) c8a    (y, get_m_x(x), get_m_y(x)), ...
    %           @(y) c8a_rho(y, get_m_x(x), get_m_y(x)));
    % fprintf('c8a_m_x\n');
    % test_grad(rand(numel(m_x), 1), ...
    %           @(y) c8a    (get_rho(x), y, get_m_y(x)), ...
    %           @(y) c8a_m_x(get_rho(x), y, get_m_y(x)));
    % fprintf('c8a_m_y\n');
    % test_grad(rand(numel(m_y), 1), ...
    %           @(y) c8a    (get_rho(x), get_m_x(x), y), ...
    %           @(y) c8a_m_y(get_rho(x), get_m_x(x), y));
    % fprintf('c8a_x\n');
    % test_grad(x, ...
    %           @(y) c8a    (get_rho(y), get_m_x(y), get_m_y(y)), ...
    %           @(y) c8a_x  (get_rho(y), get_m_x(y), get_m_y(y)));
    % % c8b
    % fprintf('c8b_rho\n');
    % test_grad(rand(numel(rho), 1), ...
    %           @(y) c8b    (y, get_m_x(x), get_m_y(x)), ...
    %           @(y) c8b_rho(y, get_m_x(x), get_m_y(x)));
    % fprintf('c8b_m_x\n');
    % test_grad(rand(numel(m_x), 1), ...
    %           @(y) c8b    (get_rho(x), y, get_m_y(x)), ...
    %           @(y) c8b_m_x(get_rho(x), y, get_m_y(x)));
    % fprintf('c8b_m_y\n');
    % test_grad(rand(numel(m_y), 1), ...
    %           @(y) c8b    (get_rho(x), get_m_x(x), y), ...
    %           @(y) c8b_m_y(get_rho(x), get_m_x(x), y));
    % fprintf('c8b_x\n');
    % test_grad(x, ...
    %           @(y) c8b    (get_rho(y), get_m_x(y), get_m_y(y)), ...
    %           @(y) c8b_x  (get_rho(y), get_m_x(y), get_m_y(y)));
    % effort
    fprintf('effort_rho\n');
    test_grad(rand(numel(rho), 1), ...
              @(y) effort    (y, get_m_x(x), get_m_y(x)), ...
              @(y) effort_rho(y, get_m_x(x), get_m_y(x)));
    fprintf('effort_m_x\n');
    test_grad(rand(numel(m_x), 1), ...
              @(y) effort    (get_rho(x), y, get_m_y(x)), ...
              @(y) effort_m_x(get_rho(x), y, get_m_y(x)));
    fprintf('effort_m_y\n');
    test_grad(rand(numel(m_y), 1), ...
              @(y) effort    (get_rho(x), get_m_x(x), y), ...
              @(y) effort_m_y(get_rho(x), get_m_x(x), y));
    fprintf('effort_x\n');
    test_grad(x, ...
              @(y) effort    (get_rho(y), get_m_x(y), get_m_y(y)), ...
              @(y) effort_x  (get_rho(y), get_m_x(y), get_m_y(y)));
    return
    % lagrange
    fprintf('lagrange_rho\n');
    test_grad(rand(numel(rho), 1), ...
              @(y) lagrange    (y, get_m_x(x), get_m_y(x)), ...
              @(y) lagrange_rho(y, get_m_x(x), get_m_y(x)));
    fprintf('lagrange_m_x\n');
    test_grad(rand(numel(m_x), 1), ...
              @(y) lagrange    (get_rho(x), y, get_m_y(x)), ...
              @(y) lagrange_m_x(get_rho(x), y, get_m_y(x)));
    fprintf('lagrange_m_y\n');
    test_grad(rand(numel(m_y), 1), ...
              @(y) lagrange    (get_rho(x), get_m_x(x), y), ...
              @(y) lagrange_m_y(get_rho(x), get_m_x(x), y));
    fprintf('lagrange_x\n');
    test_grad(x, ...
              @(x) lagrange    (get_rho(x), get_m_x(x), get_m_y(x)), ...
              @(x) lagrange_x  (get_rho(x), get_m_x(x), get_m_y(x)));
    % penalty
    fprintf('penalty_rho\n');
    test_grad(rand(numel(rho), 1), ...
              @(y) penalty    (y, get_m_x(x), get_m_y(x)), ...
              @(y) penalty_rho(y, get_m_x(x), get_m_y(x)), 'd');
    fprintf('penalty_m_x\n');
    test_grad(rand(numel(m_x), 1), ...
              @(y) penalty    (get_rho(x), y, get_m_y(x)), ...
              @(y) penalty_m_x(get_rho(x), y, get_m_y(x)));
    fprintf('penalty_m_y\n');
    test_grad(rand(numel(m_y), 1), ...
              @(y) penalty    (get_rho(x), get_m_x(x), y), ...
              @(y) penalty_m_y(get_rho(x), get_m_x(x), y));
    fprintf('penalty_x\n');
    test_grad(x, ...
              @(x) penalty    (get_rho(x), get_m_x(x), get_m_y(x)), ...
              @(x) penalty_x  (get_rho(x), get_m_x(x), get_m_y(x)));
    % obj
    fprintf('obj_x\n');
    test_grad(x, ...
              @(x) obj  (get_rho(x), get_m_x(x), get_m_y(x)), ...
              @(x) obj_x(get_rho(x), get_m_x(x), get_m_y(x)), 'd');
  end
end
