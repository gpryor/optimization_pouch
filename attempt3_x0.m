function [rho, m_x, m_y] = attempt3_x0(rho0, rhoT, m)
  gen_metadata(m);
  gen_stencils(m);

  slice_len = m_c(1) * m_c(2);
  front  = 1:slice_len;                     n_front = numel(front);
  back   = (1:slice_len) + n_c - slice_len; n_back  = numel(back);

  cvx_begin
    variables rho(n_c) m_x(n_c) m_y(n_c)
    minimize norm(c_P_f2 * f2_D2_c * m_x + c_P_f3 * f3_D3_c * m_y)    % smoothness
  subject to
    c_P_f1 * f1_D1_c * rho + c_D2_f2 * m_x + c_D3_f3 * m_y == 0;   % mass preservation
    rho(front) == rho0(:);
    rho(back) == rhoT(:);
  cvx_end
end
