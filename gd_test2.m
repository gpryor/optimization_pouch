% clear all;
setup;
addpath unit;

if ~exist('rho', 'var')
  % random start point
  rho = rand(n_c, 1); m_x = rand(n_c, 1); m_y = rand(n_c, 1); 
  psi1 = rand(n_c, 1); psi2 = rand(length(front_slice), 1); psi3 = rand(length(back_slice), 1);
  x = [rho; m_x; m_y; psi1; psi2; psi3];
end

converged = false;
while (~converged)

  L = gd_test2_L(x, rho0, rhoT, m);
  L_x = diffjac(x, ...
                @(y) gd_test2_L(y, rho0, rhoT, m), ...
                gd_test2_L(x, rho0, rhoT, m));
  x = x + 0.5 * L_x';

  % unpack x to see constraints
  rho = x(1:n_c);
  m_x = x(n_c+1:2*n_c);
  m_y = x(2*n_c+1:3*n_c);

  C7 = norm(c_P_f1 * f1_D1_c * rho + ...
            c_D2_f2 * f2_P_c * m_x + ...
            c_D3_f3 * f3_P_c * m_y);
  C8a = norm(rho(front_slice) - rho0(:));
  C8b = norm(rho( back_slice) - rhoT(:));

  [L, C7, C8a, C8b, norm(L_x)]
end

