% STEP A
% in: s{n-1}.q, s{n}.mu        out: psi
q = s{n-1}.q; mu = s{n}.mu;


RHS = c_P_f1 * f1_D1_c * (mu{1}(:) - R * q{1}(:)) + ...
               c_D2_f2 * (mu{2}(:) - R * f2_P_c * q{2}(:)) + ...
               c_D3_f3 * (mu{3}(:) - R * f3_P_c * q{3}(:));

% spatial laplacian
LAP_x = (c_D2_f2 * f2_D2_c + c_D3_f3 * f3_D3_c);



% front bdry condition
rho0_ = repmat(rho0, [1 1 m_c(end)]);
front_bdry = (rho0_(:) - mu{1} + R * q{1}) / R;



% back bdry condition
rho0_ = repmat(rho0, [1 1 m_c(end)]);
back_bdry = (rho0_(:) - mu{1} + R * q{1}) / R;



% build bdry conditions into derivative
front_slice = 1:(m_f1(3) * m_f1(2));
back_slice = (prod(m_f1)-m_f1(3)*m_f1(2)):prod(m_f1);
bdry = zeros(n_f1, 1);
bdry(front_slice) = f1_P_c(front_slice,:) * front_bdry(:);
bdry(back_slice) = f1_P_c(back_slice,:) * back_bdry(:);



% bdry conditions built in
cvx_begin
    variables psi(n_c)
    minimize  norm(  -R * ((LAP_x * psi) + c_D1_f1 * (f1_D1_c * psi + bdry)) - RHS)
cvx_end



% constraint-only problem did not work well
% cvx_begin
%     variables psi(n_c)
% subject to
%     -R * ((LAP_x * psi) + c_D1_f1 * (f1_D1_c * psi + bdry)) == RHS
% cvx_end

