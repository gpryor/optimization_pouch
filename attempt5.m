% method parameters
R = 1;

% grid, stencils
N = 64;   % grid dims
m = [N N N/4];
addpath stencils2   % do NOT use stencils/
gen_metadata(m);
gen_stencils(m);



% input data
translating_point


% solve the problem
problem.m = m;
problem.rho0 = rho0;
problem.rhoT = rhoT;
attempt5_solver(problem)
