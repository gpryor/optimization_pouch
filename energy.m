% q = {a, bx, by}
% mu = {rho, mx, my}
% grid metadata (m), iteration state (psi, a, bx, by, rho, mx, my), inputs (rho0, rhoT, R)
function L = energy(m, psi, a, bx, by, rho, mx, my, rho0, rhoT, R)    % q is a cell array
  gen_metadata(m);
  gen_stencils(m);




  grad_psi_q{1} = f1_D1_c * psi(:) - f1_P_c *  a(:);   % time
  grad_psi_q{2} = f2_D2_c * psi(:) - f2_P_c * bx(:);   % x
  grad_psi_q{3} = f3_D3_c * psi(:) - f3_P_c * by(:);   % y

  dot_mu_grad_psi_q = ...
      sum(rho(:) .* (c_P_f1 * grad_psi_q{1})) + ...
      sum( mx(:) .* (         grad_psi_q{2})) + ...
      sum( my(:) .* (         grad_psi_q{3}));

  sum_grad_psi_q_squared = ...
      sum(sum(grad_psi_q{1} .* grad_psi_q{1})) + ...
      sum(sum(grad_psi_q{2} .* grad_psi_q{2})) + ...
      sum(sum(grad_psi_q{3} .* grad_psi_q{3}));




  % % F    (inf if q outside conditions, 0 else)
  % tol = 1e-6;
  % C = a(:) + (bx(:).^2 + by(:).^2) / 2;
  % if ~all(C < tol),  F = inf;  else,  F = 0; end
  % note that q is defined on cell centers ONLY to do legendre transform
  % don't know how this puppy will play out as this is a non-linear
  % function simulating constraints
  % also note that this little bit does not play well with CVX. therefore,
  % we will resort to cvx with constraints and set F to 0
  F = 0;





  % G
  psi_cube = reshape(psi, m);
  integrand = psi_cube(:,:,1) .* rho0 - psi_cube(:,:,end) .* rhoT;
  G = sum(integrand(:));



  L = F + G + dot_mu_grad_psi_q + R/2 * sum_grad_psi_q_squared;
end
