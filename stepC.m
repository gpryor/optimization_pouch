% STEP C
% in: s{n}.mu   s{n}.psi   s{n}.q         out: s{n+1}.mu
mu = s{n}.mu; psi = s{n}.psi; q = s{n}.q;


% old gradient update
mu{1} = mu{1} + R * (c_P_f1 * f1_D1_c * psi - q{1});
mu{2} = mu{2} + R * (f2_D2_c * psi - q{2});
mu{3} = mu{3} + R * (f3_D3_c * psi - q{3});
