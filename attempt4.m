% method parameters
R = 1;

% grid, stencils
N = 16;   % grid dims
m = [N N N];
gen_metadata(m);
gen_stencils(m);

% input data
translating_point


% solve the problem
problem.m = m;
problem.rho0 = rho0;
problem.rhoT = rhoT;
attempt4_solver(problem)
