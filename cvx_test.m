% q = {a, bx, by}
% mu = {rho, mx, my}
% grid metadata (m), iteration state (psi, a, bx, by, rho, mx, my), inputs (rho0, rhoT, R)
function L = cvx_test(m, phi, a, bx, by, rho, mx, my, rho0, rhoT, R)
  gen_metadata(m);
  gen_stencils(m);



  % more conservative discretization
  grad_phi_q{1} = c_P_f1 * f1_D1_c * phi -  a;     % time
  grad_phi_q{2} = c_P_f2 * f2_D2_c * phi - bx;     % x
  grad_phi_q{3} = c_P_f3 * f3_D3_c * phi - by;     % y

  dot_mu_grad_phi_q = ...
      sum(rho .* (         grad_phi_q{1})) + ...
      sum( mx .* (         grad_phi_q{2})) + ...
      sum( my .* (         grad_phi_q{3}));

  sum_grad_phi_q_squared = ...
      sum(grad_phi_q{1} .* grad_phi_q{1} + ...
          grad_phi_q{2} .* grad_phi_q{2} + ...
          grad_phi_q{3} .* grad_phi_q{3});




  % % F    (inf if q outside conditions, 0 else)
  % tol = 1e-6;
  % C = a + (bx.^2 + by.^2) / 2;
  % if ~all(C < tol),  F = inf;  else,  F = 0; end
  % note that q is defined on cell centers ONLY to do legendre transform
  % don't know how this puppy will play out as this is a non-linear
  % function simulating constraints
  % also note that this little bit does not play well with CVX. therefore,
  % we will resort to cvx with constraints and set F to 0
  F = 0;




  % G
  phi_cube = reshape(phi, m);
  integrand = phi_cube(:,:,1) .* rho0 - phi_cube(:,:,end) .* rhoT;
  G = sum(integrand(:));





  L = F + G + dot_mu_grad_phi_q + R/2 * sum_grad_phi_q_squared;
end
