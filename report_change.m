function report_change(nm, a, b)

  a = a(:); b = b(:);
  % mn = min(a); mx = max(a);

  change = norm(b - a);
  perc_change = norm(b - a) / norm(a) * 100;

  fprintf('%s: change, %f   %% change %f\n', nm, change, perc_change);
end
