% operator: nodal grid -> face grid l
function acc = Pnf_l( l, m )

  if( l == 1 )
    op = Pnf_l_V( 2, m );
    acc = kron( speye( m(1)+1 ), op );
  else
    op = Pnf_l_V( 1, m );
    acc = kron( op, speye( m(2)+1 ) );
  end

end
