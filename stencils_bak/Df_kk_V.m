function A = Df_kk_V( k, m )
  one = ones( m(k), 1 );
  A = spdiags( [ -one one ], [ 0 1 ], m(k), m(k) + 1 );
end
