% operator: faces -> cells
function acc = Pfc_l( l, m )

  op = Pfc_l_V( l, m );

  if( l == 1 ) acc = op; else acc = speye( m(1) ); end;
  
  for i=2:length(m)
    if( i == l ) acc = kron( acc, op ); else
      acc = kron( acc, speye( m(i) ) ); end
  end
  
end
