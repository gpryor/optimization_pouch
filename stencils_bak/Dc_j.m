function acc = Dc_j( j, m )
  
  op = Dc_j_V( j, m );
  
  if( j == 1 ) acc = op; else acc = speye( m(1) ); end;

  for i=2:length(m)
    if( i == j ) acc = kron( acc, op ); else
      acc = kron( acc, speye( m(i) ) ); end
  end

end
