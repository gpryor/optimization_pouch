% operator: faces -> cells
function acc = Df_kk( k, m )
  
  op = Df_kk_V( k, m );
  
  if( k == 1 ) acc = op; else acc = speye( m(1) ); end;
  
  for i=2:length(m)
    if( i == k ) acc = kron( acc, op ); else
      acc = kron( acc, speye( m(i) ) ); end
  end
  
end
