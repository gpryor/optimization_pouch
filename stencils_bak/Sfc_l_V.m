function A = Sfc_l_V( l, m, offset )
  one = 1/2 * ones( m(l) + 1, 1 );
  A = spdiags( one, offset, m(l), m(l) + 1 );
end
