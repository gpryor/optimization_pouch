kappa = -rho;
b_x = -m_x ./ kappa;
b_y = -m_y ./ kappa;
a = -(b_x.^2 + b_y.^2) ./ 2;
