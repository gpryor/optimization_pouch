function L = gd_test_L(x, rho0, rhoT, m)
  
  % meta data
  gen_metadata(m);
  gen_stencils(m);
  slice_len = m_c(1) * m_c(2);
  front_slice  = 1:slice_len;
  back_slice   = (1:slice_len) + n_c - slice_len;

  % unpack x
  rho = x(1:n_c);
  m_x = x(n_c+1:2*n_c);
  m_y = x(2*n_c+1:end);

  % energy
  % L = rho(1) ^ 2;
  % L = sum(rho.^2);

  L = sum((c_P_f1 * f1_D1_c * rho + ...
           c_D2_f2 * f2_P_c * m_x + ...
           c_D3_f3 * f3_P_c * m_y).^2) + ...
      sum((rho(front_slice) - rho0(:)).^2) + ...
      sum((rho( back_slice) - rhoT(:)).^2);
end
