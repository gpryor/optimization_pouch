function auglag_solver
  % augmented lagrangian approach of:
  % http://www.google.com/url?sa=t&source=web&cd=1&ved=0CBcQFjAA&url=http%3A%2F%2Fwww.cs.ubc.ca%2F~ascher%2F542%2Fchap10.pdf&ei=_PNgTNaZIoGClAfvpJGYCw&usg=AFQjCNH4JwbSHtA1fQFw08oa2dC5IrkwWQ
  % attempting to solve linear objective with a linear constraint

  % settings
  a = 2;
  c = [-0.3; 0.1];
  b = [-0.5; 1];
  d = [0.5; -1; -1];

  % start from random point
  x = rand(2,1);

  % checks
  % check objective, gradient
  test_grad(x, @obj, @grad_obj);
  % check constraint, gradient
  test_grad(x, @constraint, @grad_constraint);
  % check aug. lag.,  gradient
  phi = rand; l = rand;
  test_grad(x, ...
            @(y) aug_lagrangian(y, l, phi), ...
            @(y) grad_aug_lagrangian(y, l, phi));

  % EXHIBIT 3
  % attempt at hitting the constraint using augmented lagrangian
  x = rand(2, 1);
  h = inline('0.5 * x + -y + -1', 'x', 'y');

  % augmented lagrangian solver
  phi = 1; l = 0; tau_inner = 1e-3; tau_outer = 1e-3;
  while (true)

    % gradient descent inner loop   - - going to need a convergence test
    L = aug_lagrangian(x, l, phi);
    while (true)

      % armijo rule line search parameters
      beta = 0.5; alpha = 1e-4; f = @(x) aug_lagrangian(x, l, phi);     % constant settings

      % line search
      L_x = grad_aug_lagrangian(x, l, phi);
      m = 0;
      while m <= 5
        lambda = beta ^ m;
        accept = f(x - lambda * L_x) - f(x) < -alpha * lambda * sum(L_x.^2);
        if accept, break; end
        m = m + 1;
      end

      % stop if could not make progress
      if m == 6, break; end

      % else, accept 
      x = x - lambda * L_x;
      
      % status
      L_ = aug_lagrangian(x, l, phi);
      fprintf('x:(%f,%f)    L: %f   obj: %f   const. %f\n', ...
              x(1), x(2), L_, obj(x(1:2)), constraint(x(1:2)));
      plot(x(1), x(2), 'rx', 'MarkerSize', 10, 'Linewidth', 3); hold on;
      implot2(h, 'LineWidth', 3);
      plot(-c(1), -c(2), 'gx', 'MarkerSize', 10, 'Linewidth', 3);
      plot( [0 b(1)],  [0 b(2)], 'b-', 'MarkerSize', 10, 'Linewidth', 3); hold off;
      axis([-4, 4, -4, 4]);
      drawnow;

      % stop on convergence (inner loop)
      if (norm(L_x) < tau_inner), break; end
    end

    % stop on convergence (inner loop)
    % don't really know what to do here
    disp('inner loop converged');

    % adjust l
    l = l - 1/phi * constraint(x);

    % adjust phi
    phi = phi * 0.3;

    % break on convergence
    L_ = aug_lagrangian(x, l, phi);
    if (norm(L - L_) < tau_outer), break; end

  end
  disp('outer loop converged');






  % minimize using gradient descent on augmented lagrangian
  % x = [x; l, phi];
  % length(l) = 1   (we have only one constraint)
  function L = aug_lagrangian(x, l, phi)
    L = obj(x) - l * constraint(x) + 1 ./ (2 * phi) * constraint(x).^2;
  end
  function dL = grad_aug_lagrangian(x, l, phi)
    dL = grad_obj(x) - l * grad_constraint(x) + 1 ./ (phi) * grad_constraint(x) * constraint(x);
  end



  % base functions (objective and constraint)
  function L = obj(x)
    L = a * (b(1) * x(1) + b(2) + x(2));
  end

  function dL = grad_obj(x)
    dL = a * b;
  end

  function C = constraint(x)
    C = d(1) * x(1) + d(2) * x(2) + d(3);
  end

  function dC = grad_constraint(x)
    dC = d(1:2);
  end
end
