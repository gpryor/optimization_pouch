function gradmag_solver3
  % we seek to find out if the magnitude/gradient method can handle linear
  % objective functions AND LINEAR CONSTRAINTS
  % this was a TOTAL FAIL. now moving to the augmented lagrangian approach
  % of: http://www.google.com/url?sa=t&source=web&cd=1&ved=0CBcQFjAA&url=http%3A%2F%2Fwww.cs.ubc.ca%2F~ascher%2F542%2Fchap10.pdf&ei=_PNgTNaZIoGClAfvpJGYCw&usg=AFQjCNH4JwbSHtA1fQFw08oa2dC5IrkwWQ

  % settings
  alpha = 2;
  c = [-0.3; 0.1];
  b = [-0.5; 1];
  d = [-2; 3; -5];

  % start from random point
  x = rand(2,1);

  % checks
  % check objective, gradient
  test_grad(x, @obj, @grad_obj);
  % check constraint, gradient
  test_grad(x, @constraint, @grad_constraint);
  % % check aug. lag.,  gradient
  % test_grad(x, @aug_lagrangian, @grad_aug_lagrangian);

  % check lagrange multiplier obj,  gradient
  % (can't use x because the following also requires a lambda)
  test_grad(rand(3,1), @obj_w_lagrange, @grad_obj_w_lagrange);
  % check lagrange multiplier obj, hessian
  test_grad(rand(3,1), @grad_obj_w_lagrange, @grad2_obj_w_lagrange);
  % check mag grad and grad mag grad
  test_grad(rand(3,1), @mag_grad_obj_w_lagrange, @grad_mag_grad_obj_w_lagrange);





  % EXHIBIT 3
  % attempt at hitting the constraint by minimizing magnitude of gradient
  x = rand(3, 1);    % there is a lambda now in the mix
  h = inline('-2 * x + 3 * y + -5', 'x', 'y');
  iter = 0; converged = false;
  while (~converged)
    L = mag_grad_obj_w_lagrange(x);
    L_x = grad_mag_grad_obj_w_lagrange(x);
    x = x - 0.001 * L_x;

    fprintf('x:(%f,%f)    L: %f   obj: %f   const. %f\n', ...
            x(1), x(2), L, obj(x(1:2)), constraint(x(1:2)));

    plot(x(1), x(2), 'rx', 'MarkerSize', 10, 'Linewidth', 3); hold on;
    implot2(h, 'LineWidth', 3);
    plot(-c(1), -c(2), 'gx', 'MarkerSize', 10, 'Linewidth', 3);
    plot( [0 b(1)],  [0 b(2)], 'b-', 'MarkerSize', 10, 'Linewidth', 3); hold off;
    axis equal;
    drawnow;
  end




  % magnitude of gradient of lagrangian w/ lagrange multipliers
  function L = obj_w_lagrange(x)
    % lambda is x(3)
    L = obj(x(1:2)) - x(3) * constraint(x(1:2));
  end
  function dL = grad_obj_w_lagrange(x)
    % lambda is x(3)
    dLBYdx = grad_obj(x(1:2)) - x(3) * grad_constraint(x(1:2));
    dLBYdlambda = -constraint(x(1:2));
    dL = [dLBYdx; dLBYdlambda];
  end
  function dL = grad2_obj_w_lagrange(x)
    gc = grad_constraint(x(1:2));
    dL = [-x(3) * 2 * d(1) * d(1)  , -x(3) * 2 * d(2) * d(1)   , -gc(1);
          -x(3) * 2 * d(2) * d(1)  , -x(3) * 2 * d(2) * d(2)   , -gc(2);
                       -gc(1)      ,          -gc(2)           ,     0];
  end
  function L = mag_grad_obj_w_lagrange(x)
    L = sum(grad_obj_w_lagrange(x).^2);
  end
  function dL = grad_mag_grad_obj_w_lagrange(x)
    dL = 2 * grad2_obj_w_lagrange(x) * grad_obj_w_lagrange(x);
  end



  % minimize using gradient descent on augmented lagrangian
  function L = aug_lagrangian(x)
    L = obj(x) + constraint(x)^2;
  end
  function dL = grad_aug_lagrangian(x)
    dL = grad_obj(x) + 2 * grad_constraint(x) * constraint(x);
  end



  % base functions (objective and constraint)
  function L = obj(x)
    % L = alpha * sum(x .^ 2);
    L = alpha * (b(1) * x(1) + b(2) + x(2));
  end

  function dL = grad_obj(x)
    % dL = alpha * 2 * x;
    dL = alpha * b;
  end

  function C = constraint(x)
    % x = (x + c);
    % C = x' * x - 4;
    C = (d(1) * x(1) + d(2) * x(2) + d(3))^2;
  end

  function dC = grad_constraint(x)
    % dC = 2 * x + 2 * c;
    dC = 2 * (d(1) * x(1) + d(2) * x(2) + d(3)) * d(1:2);
  end
end
