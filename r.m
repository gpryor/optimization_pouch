% % grid, stencils
% N = 3;
% m   = [N N N];
% gen_metadata(m);
% gen_stencils(m);
% return



% setup
cvx_quiet true
% method parameters
R = 1;

% grid, stencils
N = 8;   % grid dims
m   = [N N N];
gen_metadata(m);
gen_stencils(m);

% grid meta data
slice_len = m_c(1) * m_c(2);
front_slice  = 1:slice_len;
back_slice   = (1:slice_len) + n_c - slice_len;


% input data
translating_point



% solve
% initial iteration state
init_mu;     % checks out fully
init_q;      % appears to oscillate too much



% diagnostics
% [X, Y] = meshgrid(1:m_c(2), 1:m_c(3));
% % X = repmat(X, [1 1 m_c(3)]);
% % Y = repmat(Y, [1 1 m_c(3)]);
% dx = reshape(mu{2}, m_c);
% dy = reshape(mu{3}, m_c);
% quiver(X, Y, sum(dx, 3), sum(dy, 3));
% plotGrid(Y + sum(dy, 3), X + sum(dx, 3), 'linewidth', 2, 'gridx', 8, 'gridy', 8);
% return




s{2}.mu = mu;
s{1}.q = q;
s{1}.psi = rand(m_c, class(mu{1}));

% iterate
e = [];      % e is for error
for n = 2:100
  
  % carry state over from last iteration
  s{n  }.psi = s{n-1}.psi;
  s{n  }.q   = s{n-1}.q;
  s{n+1}.mu  = s{n}.mu;

  if ~isempty(s{n}.psi)
    IL = L(s{n}.q, reshape(s{n}.psi, m_c), s{n+1}.mu, rho0, rhoT, R); 
  else
    IL = 0;
  end
  stepA;      s{n  }.psi = psi;
  AL = L(s{n}.q, reshape(s{n}.psi, m_c), s{n+1}.mu, rho0, rhoT, R);
  stepB;      s{n  }.q   = q;
  BL = L(s{n}.q, reshape(s{n}.psi, m_c), s{n+1}.mu, rho0, rhoT, R);

  [AL BL]

  return;    % lets get stepB working

  stepC;      s{n+1}.mu  = mu;
  CL = L(s{n}.q, reshape(s{n}.psi, m_c), s{n+1}.mu, rho0, rhoT, R);

  [IL AL BL CL]

  % gvolume(reshape(s{n+1}.mu{1}, m_c));

  % energy (probably wrong)
  e = [e L(s{n}.q, reshape(s{n}.psi, m_c), s{n+1}.mu, rho0, rhoT, R)];

  % understand what state is changing per iteration
  if 0 && n > 2
    report_change('psi', s{n-1}.psi, s{n}.psi);
    report_change('q{1}', s{n-1}.q{1}, s{n}.q{1});
    report_change('mu{1}', s{n+1}.mu{1}, s{n}.mu{1});
    report_change('mu{2}', s{n+1}.mu{2}, s{n}.mu{2});
    report_change('mu{3}', s{n+1}.mu{3}, s{n}.mu{3});
    report_change('e', e(end), e(end-1));
    fprintf('e prev  %f, e curr   %f\n', e(end-1), e(end));
  end
end
