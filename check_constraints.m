% check optimality conditions


% optimality contraint 4 (first slice matches input)
rho_cube = reshape(rho, m);
T = rho_cube(:,:,1) - rho0;
c4 = norm(T);
c4_r = norm(rho0(:));


% optimality constraint 5 (last slice matches input)
rho_cube = reshape(rho, m);
T = rho_cube(:,:,end) - rhoT;
c5 = norm(T);
c5_r = norm(rhoT);


% optimality contraint 2.1, 2.2 (v = grad of psi; our convex function)
c2_1 = norm(m_x ./ (f2_P_c * rho) - f2_D2_c * psi);
c2_2 = norm(m_y ./ (f3_P_c * rho) - f3_D3_c * psi);

% agrees with constraint we have above
% T1_ = m_y ./ (f3_P_c * rho);
% T2_ = f3_D3_c * psi;
% plot(T1_); hold on; plot(T2_, 'r'); hold off;
% legend({'m_y and rho', 'dpsi/dy'}); title('constraint 2.2');
% drawnow;

% % show gradients from each objective
% G1 = m_y ./ (f3_P_c * rho) - f3_D3_c * psi;
% G2 = f3_D3_c * psi(:) - f3_P_c * by(:);
% plot(G1); hold on; plot(G2, 'r'); hold off;
% legend({'d/m_x of 1st L', 'd/m_x of 2nd L'}); title('gradient comparison');
% drawnow;


% optimality constraint 3   (mass preservation)
c3 = norm(c_P_f1 * f1_D1_c * rho + c_P_f2 * f2_D2_c * m_x + c_P_f3 * f3_D3_c * m_y);

% grad_L_psi = diffjac(psi, ...
%                      @(x) cvx_test(m, x, a, bx, by, rho, m_x, m_y, rho0, rhoT, R), ...
%                           cvx_test(m, psi, a, bx, by, rho, m_x, m_y, rho0, rhoT, R));

% plot(c_P_f1 * f1_D1_c * rho + c_P_f2 * f2_D2_c * m_x + c_P_f3 * f3_D3_c * m_y);
% hold on;
% plot(-grad_L_psi, 'r');
% hold off;
% legend({'d/dpsi L', '-d/dpsi cvx_test'});
% drawnow;


% optimality constraint 1 (least work in original objective)
T1 = c_P_f1 * f1_D1_c * psi;
T2 = ((c_P_f2 * m_x) .^ 2 + (c_P_f3 * m_y) .^ 2) ./ (2 * (rho .^ 2));

T = T1 + T2;
c1 = norm(T);
c1_r1 = norm(T1);
c1_r2 = norm(T2);


% % v = [0  600.0000   -0.0002    0.00005];
% plot(T1); hold on; plot(-T2, 'r'); hold off;
% % axis(v);
% legend({'psi', 'mu'}); title('constraint 1');
% drawnow;


% relative error printout
% fprintf('c1 %f (%f/%f), c4 %0.2f (%0.2f), c5 %0.2f (%0.2f)\n', ...
%         c1, c1_r1, c1_r2, c4, c4_r, c5, c5_r);

% absolute error printout
fprintf('c1 %f, c2.1 %f, c2.2 %f, c3 %f, c4 %0.2f, c5 %0.2f\n', c1, c2_1, c2_2, c3, c4, c5);
