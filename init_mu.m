% initial rho and m
cvx_begin
  variables rho(n_c) m_x(n_c) m_y(n_c)
  minimize  norm(c_D2_f2 * f2_P_c * m_x + c_D3_f3 * f3_P_c * m_y)
subject to
  c_P_f1 * f1_D1_c * rho + c_D2_f2 * f2_P_c * m_x + c_D3_f3 * f3_P_c * m_y == 0
  rho(front) == rho0(:);
  rho(back) == rhoT(:);
cvx_end

