% grid dimensions (3D)
N = 40;

% stencil creation
m   = [N N N];
gen_metadata(m);
gen_stencils(m);

% DIVERGENCE LOOKS CORRECT!!!
% make sure that we're actually computing the divergence
rng = [];
rng{1} = ((1:m_c(1)) - 1) / m_c(1);
rng{2} = ((1:m_c(2)) - 2) / m_c(2);
rng{3} = ((1:m_c(3)) - 3) / m_c(3);
[X, Y, Z] = meshgrid(rng{:});   % cell centered
fn_1 = sin(X); fn_2 = sin(Y) * 0; fn_3 = sin(Z) * 0;

% divergence should be (cell centered)
div_gold = cos(X) + cos(Y) * 0 + cos(Z) * 0;

% The following is the correct divergence!!!
div_new = c_D2_f2 * (f2_P_c * fn_1(:)) * m_c(1) + ...
          c_D3_f3 * (f3_P_c * fn_2(:)) * m_c(2) + ...
          c_D1_f1 * (f1_P_c * fn_3(:)) * m_c(3);
plot(div_gold(:)); hold on; plot(div_new(:), 'r'); hold off;

tmp = reshape(div_new, m_c);
gvolume(tmp)
