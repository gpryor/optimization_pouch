% attempt to minimize the L2 MKP directly via gradient descent


% start with valid state
clear all;
setup;
init_mu;
init_q;
a = q{1}; bx = q{2}; by = q{3};
rho = mu{1}; mx = mu{2}; my = mu{3};


cvx_begin
  variables rho(n_c) mx(n_c) my(n_c)
  minimize sum((mx.^2 + my.^2) ./ (2 * rho))
subject to
  c_P_f1 * f1_D1_c * rho + c_D2_f2 * f2_P_c * m_x + c_D3_f3 * f3_P_c * m_y == 0;
  rho(front_slice) == rho0(:);
  rho(back_slice) == rhoT(:);
cvx_end


% constraint check
fcheck(rho(front_slice), rho0(:));
fcheck(rho(back_slice), rhoT(:));
