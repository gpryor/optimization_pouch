function attempt4_solver(problem)

  % unpack problem, grid metadata, stencils
  % problem
  m = problem.m; rho0 = problem.rho0; rhoT = problem.rhoT;
  % grid metadata
  s = gen_metadata(m, 'struct');
  m = s.m; m_c = s.m_c; m_n = s.m_n;
  m_f1 = s.m_f1; m_f2 = s.m_f2; m_f3 = s.m_f2;
  n_c  = s.n_c; n_n  = s.n_n; n_f1 = s.n_f1; n_f2 = s.n_f2; n_f3 = s.n_f3;
  slice_len = m_c(2) * m_c(3);
  front  = 1:slice_len;                     n_front = numel(front);
  back   = (1:slice_len) + n_c - slice_len; n_back  = numel(back);
  % stencils
  s = gen_stencils(m, 'struct');
  [v, d] = max(m);
  s1 = v / m(1); s2 = v / m(2); s3 = v / m(3);
  f1_D1_c = s1 * s.f1_D1_c; f2_D2_c = s2 * s.f2_D2_c; f3_D3_c = s3 * s.f3_D3_c;
  c_D1_f1 = s1 * s.c_D1_f1; c_D2_f2 = s2 * s.c_D2_f2; c_D3_f3 = s3 * s.c_D3_f3;
  c_P_f1  = s.c_P_f1;  c_P_f2  = s.c_P_f2;  c_P_f3  = s.c_P_f3;
  f1_P_c  = s.f1_P_c;  f2_P_c  = s.f2_P_c;  f3_P_c  = s.f3_P_c;




  % random starting point
  [rho, m_x, m_y, a, b_x, b_y] = deal(rand(n_c, 1));
  % % smooth starting point
  % [rho, m_x, m_y] = attempt3_x0(rho0, rhoT, m);
  % [a, b_x, b_y] = deal(rand(n_c, 1));
  % identity m_x, m_y starting point
  % rho = repmat(rho0, [1 1 m(1)]); rho = rho(:);
  % [m_x m_y] = deal(zeros(n_c, 1));
  % [a, b_x, b_y] = deal(rand(n_c, 1));
  x = [rho; m_x; m_y];

  % good q based on starting point
  kappa = -get_rho(x);
  b_x = -get_m_x(x) ./ kappa;
  b_y = -get_m_y(x) ./ kappa;
  a = -(b_x.^2 + b_y.^2) ./ 2;

  % augmented lagrangian solver
  % estimates for langrange multipliers, penalities
  [u_1, u_2, u_3] = deal(ones(n_c, 1)); [l_1, l_2, l_3] = deal(zeros(n_c, 1)); 

  % % possible unit testing at start point
  % [l_1 l_2 l_3] = deal(rand(n_c, 1)); unit_test; return

  % convergence tolerances
  tau_inner = 1e-2; tau_outer = 1e-2;
  while (true)

    L0 = obj(get_rho(x), get_m_x(x), get_m_y(x));
    iter = 0;
    while (true)
      iter = iter + 1;

      % q; effort term as sup_q(convex function)
      kappa = -get_rho(x);
      b_x = -get_m_x(x) ./ kappa;
      b_y = -get_m_y(x) ./ kappa;
      a = -(b_x.^2 + b_y.^2) ./ 2;

      % make sure that approximation matches effort
      true_effort = (get_m_x(x).^2 + get_m_y(x).^2) ./ (2 * get_rho(x));
      approx_effort = a .* get_rho(x) + b_x .* get_m_x(x) + b_y .* get_m_y(x);
      % plot(true_effort); hold on; plot(approx_effort, 'r'); hold off; pause;
      fcheck(true_effort, approx_effort);

      % armijo rule line search parameters (constant settings)
      beta = 0.5^3; alpha = 1e-4; 
      f = @(x) obj(get_rho(x), get_m_x(x), get_m_y(x));

      old_f = f(x);

      % line search
      L_x = obj_x(get_rho(x), get_m_x(x), get_m_y(x));
      m = 1;
      while m <= 6
        lambda = beta ^ m;
        accept = f(x - lambda * L_x) - f(x) < -alpha * lambda * sum(L_x.^2);
        if accept, break; end
        m = m + 1;
      end

      % stop if could not make progress
      if m == 7, break; end

      % else, accept
      x = x - lambda * L_x;

      % stop on convergence (inner loop)
      if (norm(lambda * L_x) < tau_inner), break; end

      % status
      if ~mod(iter, 1), status; end
    end

    % stop on convergence (inner loop)
    % don't really know what to do here
    disp('inner loop converged');
    % pause

    % adjust l     used to be (l = l - 1/phi * constraint(x));
    l_1 = l_1 - (1 ./ u_1) .* c7 (get_rho(x), get_m_x(x), get_m_y(x));
    l_2 = l_2 - (1 ./ u_2) .* c8a(get_rho(x), get_m_x(x), get_m_y(x));
    l_3 = l_3 - (1 ./ u_3) .* c8b(get_rho(x), get_m_x(x), get_m_y(x));

    % adjust u
    u_1 = u_1 * 0.3;
    u_2 = u_2 * 0.3;
    u_3 = u_3 * 0.3;

    % only possibly break if converged to constraints
    c7_at_x  = c7 (get_rho(x), get_m_x(x), get_m_y(x));
    c8a_at_x = c8a(get_rho(x), get_m_x(x), get_m_y(x));
    c8b_at_x = c8b(get_rho(x), get_m_x(x), get_m_y(x));
    if norm(c7_at_x) < 1e-5 && norm(c8a_at_x) < 1e-5 && norm(c8b_at_x) < 1e-5
      % break on convergence
      L_ = obj(get_rho(x), get_m_x(x), get_m_y(x));
      if (norm(L_) / norm(L0) < tau_outer), break; end
    end

    % break on no progress
    if (iter == 1) break; end
  end
  disp('outer loop converged');
  status

  


  function status
    L = obj(get_rho(x), get_m_x(x), get_m_y(x));
    true_effort = sum((get_m_x(x).^2 + get_m_y(x).^2) ./ (2 * get_rho(x)));
    c7_at_x = c7 (get_rho(x), get_m_x(x), get_m_y(x));
    c8a_at_x = c8a(get_rho(x), get_m_x(x), get_m_y(x));
    c8b_at_x = c8b(get_rho(x), get_m_x(x), get_m_y(x));
    fprintf('L: %f  effort: %f   c7: %f  c8a: %f  c8b: %f    m: %d\n', ...
            L, true_effort, norm(c7_at_x), norm(c8a_at_x), norm(c8b_at_x), m);

    BLAH = get_rho(x);
    BLAH = reshape(BLAH, [m_f3(1) m_f3(2) m_f3(3)]);
    gvolume(BLAH);
    % plot(get_rho(x)); drawnow;

    % rho = get_rho(x);
    % plot(rho(1:16:end));
    % rho = reshape(get_rho(x), [m_c(3) m_c(2) m_c(1)]);
    % imagesc(rho(:,:,end));

    % BLAH = reshape(get_rho(x), [m_c(3) m_c(2) m_c(1)]);
    % j = 1;
    % for i=1:4   %linspace(1, m_f3(3), 8)
    %   subplot(5,2,j); j = j + 1;
    %   imagesc(BLAH(:,:,i)); 
    % end
    % drawnow;
  end



  % augmented lagrangian, gradient for OMT
  function L = obj(rho, m_x, m_y)
    L = effort(rho, m_x, m_y) - lagrange(rho, m_x, m_y) + penalty(rho, m_x, m_y);
  end
  function L = obj_x(rho, m_x, m_y)
    L = effort_x(rho, m_x, m_y) - lagrange_x(rho, m_x, m_y) + penalty_x(rho, m_x, m_y);
  end


  % effort
  function L = effort(rho, m_x, m_y), L = dot(rho, a) + dot(m_x, b_x) + dot(m_y, b_y); end


  % lagrange
  function L = lagrange(rho, m_x, m_y)
    L = dot(l_1, c7 (rho, m_x, m_y)) + ...
        dot(l_2, c8a(rho, m_x, m_y)) + ...
        dot(l_3, c8b(rho, m_x, m_y));
  end


  % penalty
  function L = penalty(rho, m_x, m_y)
    L = dot(1 ./ (2 * u_1),  c7(rho, m_x, m_y) .^ 2) + ...
        dot(1 ./ (2 * u_2), c8a(rho, m_x, m_y) .^ 2) + ...
        dot(1 ./ (2 * u_3), c8b(rho, m_x, m_y) .^ 2);
  end


  % constraints
  function L = c7(rho, m_x, m_y)
    L = c_P_f1 * f1_D1_c * rho + c_D2_f2 * m_x + c_D3_f3 * m_y;
  end
  function L = c8a(rho, m_x, m_y)
    L = [rho(front) - rho0(:); zeros(n_c - n_front, 1)];
  end
  function L = c8b(rho, m_x, m_y)
    L = [zeros(n_c - n_back, 1); rho(back) - rhoT(:)];
  end


  % c7 gradient
  function L = c7_rho(rho, m_x, m_y), L = c_P_f1 * f1_D1_c; end
  function L = c7_m_x(rho, m_x, m_y), L = c_D2_f2; end
  function L = c7_m_y(rho, m_x, m_y), L = c_D3_f3; end
  function L = c7_x  (rho, m_x, m_y)
    tmp = map(@(fn) fn(rho, m_x, m_y), {@c7_rho, @c7_m_x, @c7_m_y});
    L = cat(2, tmp{:});
  end


  % c8a gradient
  function L = c8a_rho(rho, m_x, m_y)
    L = speye(n_front);
    L(n_c, numel(rho)) = 0;
  end 
  function L = c8a_m_x(rho, m_x, m_y), L = sparse(n_c, numel(m_x)); end
  function L = c8a_m_y(rho, m_x, m_y), L = sparse(n_c, numel(m_y)); end
  function L = c8a_x  (rho, m_x, m_y)
    tmp = map(@(fn) fn(rho, m_x, m_y), {@c8a_rho, @c8a_m_x, @c8a_m_y});
    L = cat(2, tmp{:});
  end


  % c8b gradient
  function L = c8b_rho(rho, m_x, m_y)
    d = [zeros(n_c - n_back, 1); ones(n_back, 1)];
    L = spdiags(d, 0, n_c, numel(rho));
  end
  function L = c8b_m_x(rho, m_x, m_y), L = sparse(n_c, numel(m_x)); end
  function L = c8b_m_y(rho, m_x, m_y), L = sparse(n_c, numel(m_y)); end
  function L = c8b_x  (rho, m_x, m_y)
    tmp = map(@(fn) fn(rho, m_x, m_y), {@c8b_rho, @c8b_m_x, @c8b_m_y});
    L = cat(2, tmp{:});
  end


  % effort gradient
  function L = effort_rho(rho, m_x, m_y), L = a; end
  function L = effort_m_x(rho, m_x, m_y), L = b_x; end
  function L = effort_m_y(rho, m_x, m_y), L = b_y; end
  function L = effort_x  (rho, m_x, m_y)
    tmp = map(@(fn) fn(rho, m_x, m_y), {@effort_rho, @effort_m_x, @effort_m_y});
    L = cat(1, tmp{:});
  end


  % lagrange gradient
  function L = lagrange_rho(rho, m_x, m_y)
    L = c7_rho(rho, m_x, m_y)' * l_1 + ...
        c8a_rho(rho, m_x, m_y)' * l_2 + ...
        c8b_rho(rho, m_x, m_y)' * l_3;
  end
  function L = lagrange_m_x(rho, m_x, m_y)
    L =  c7_m_x(rho, m_x, m_y)' * l_1 + ...
        c8a_m_x(rho, m_x, m_y)' * l_2 + ...
        c8b_m_x(rho, m_x, m_y)' * l_3;
  end
  function L = lagrange_m_y(rho, m_x, m_y)
    L =  c7_m_y(rho, m_x, m_y)' * l_1 + ...
        c8a_m_y(rho, m_x, m_y)' * l_2 + ...
        c8b_m_y(rho, m_x, m_y)' * l_3;
  end
  function L = lagrange_x  (rho, m_x, m_y)
    tmp = map(@(fn) fn(rho, m_x, m_y), {@lagrange_rho, @lagrange_m_x, @lagrange_m_y});
    L = cat(1, tmp{:});
  end


  % penalty gradient
  function L = penalty_rho(rho, m_x, m_y)
    L =  c7_rho(rho, m_x, m_y)' * ( c7(rho, m_x, m_y) .* (1 ./ u_1)) + ...
        c8a_rho(rho, m_x, m_y)' * (c8a(rho, m_x, m_y) .* (1 ./ u_2)) + ...
        c8b_rho(rho, m_x, m_y)' * (c8b(rho, m_x, m_y) .* (1 ./ u_3));
  end
  function L = penalty_m_x(rho, m_x, m_y)
    L =  c7_m_x(rho, m_x, m_y)' * ( c7(rho, m_x, m_y) .* (1 ./ u_1)) + ...
        c8a_m_x(rho, m_x, m_y)' * (c8a(rho, m_x, m_y) .* (1 ./ u_2)) + ...
        c8b_m_x(rho, m_x, m_y)' * (c8b(rho, m_x, m_y) .* (1 ./ u_3));
  end
  function L = penalty_m_y(rho, m_x, m_y)
    L =  c7_m_y(rho, m_x, m_y)' * ( c7(rho, m_x, m_y) .* (1 ./ u_1)) + ...
        c8a_m_y(rho, m_x, m_y)' * (c8a(rho, m_x, m_y) .* (1 ./ u_2)) + ...
        c8b_m_y(rho, m_x, m_y)' * (c8b(rho, m_x, m_y) .* (1 ./ u_3));
  end
  function L = penalty_x  (rho, m_x, m_y)
    tmp = map(@(fn) fn(rho, m_x, m_y), {@penalty_rho, @penalty_m_x, @penalty_m_y});
    L = cat(1, tmp{:});
  end


  % utility
  function rho = get_rho(x), rho = x(          1:    n_c); end
  function m_x = get_m_x(x), m_x = x(    n_c + 1:2 * n_c); end
  function m_y = get_m_y(x), m_y = x(2 * n_c + 1:3 * n_c); end
  function varargout = map(fn, varargin)
    varargout = cell(1,max(1,nargout));
    [varargout{:}] = cellfun(fn, varargin{:}, 'Un', 0);
  end


  % unit tests
  function unit_test
    % c7
    fprintf('c7_rho\n');
    test_grad(rand(numel(rho), 1), ...
              @(y) c7    (y, get_m_x(x), get_m_y(x)), ...
              @(y) c7_rho(y, get_m_x(x), get_m_y(x)));
    fprintf('c7_m_x\n');
    test_grad(rand(numel(m_x), 1), ...
              @(y) c7    (get_rho(x), y, get_m_y(x)), ...
              @(y) c7_m_x(get_rho(x), y, get_m_y(x)));
    fprintf('c7_m_y\n');
    test_grad(rand(numel(m_y), 1), ...
              @(y) c7    (get_rho(x), get_m_x(x), y), ...
              @(y) c7_m_y(get_rho(x), get_m_x(x), y));
    fprintf('c7_x\n');
    test_grad(x, ...
              @(y) c7    (get_rho(y), get_m_x(y), get_m_y(y)), ...
              @(y) c7_x  (get_rho(y), get_m_x(y), get_m_y(y)));
    % c8a
    fprintf('c8a_rho\n');
    test_grad(rand(numel(rho), 1), ...
              @(y) c8a    (y, get_m_x(x), get_m_y(x)), ...
              @(y) c8a_rho(y, get_m_x(x), get_m_y(x)));
    fprintf('c8a_m_x\n');
    test_grad(rand(numel(m_x), 1), ...
              @(y) c8a    (get_rho(x), y, get_m_y(x)), ...
              @(y) c8a_m_x(get_rho(x), y, get_m_y(x)));
    fprintf('c8a_m_y\n');
    test_grad(rand(numel(m_y), 1), ...
              @(y) c8a    (get_rho(x), get_m_x(x), y), ...
              @(y) c8a_m_y(get_rho(x), get_m_x(x), y));
    fprintf('c8a_x\n');
    test_grad(x, ...
              @(y) c8a    (get_rho(y), get_m_x(y), get_m_y(y)), ...
              @(y) c8a_x  (get_rho(y), get_m_x(y), get_m_y(y)));
    % c8b
    fprintf('c8b_rho\n');
    test_grad(rand(numel(rho), 1), ...
              @(y) c8b    (y, get_m_x(x), get_m_y(x)), ...
              @(y) c8b_rho(y, get_m_x(x), get_m_y(x)));
    fprintf('c8b_m_x\n');
    test_grad(rand(numel(m_x), 1), ...
              @(y) c8b    (get_rho(x), y, get_m_y(x)), ...
              @(y) c8b_m_x(get_rho(x), y, get_m_y(x)));
    fprintf('c8b_m_y\n');
    test_grad(rand(numel(m_y), 1), ...
              @(y) c8b    (get_rho(x), get_m_x(x), y), ...
              @(y) c8b_m_y(get_rho(x), get_m_x(x), y));
    fprintf('c8b_x\n');
    test_grad(x, ...
              @(y) c8b    (get_rho(y), get_m_x(y), get_m_y(y)), ...
              @(y) c8b_x  (get_rho(y), get_m_x(y), get_m_y(y)));
    % effort
    fprintf('effort_rho\n');
    test_grad(rand(numel(rho), 1), ...
              @(y) effort    (y, get_m_x(x), get_m_y(x)), ...
              @(y) effort_rho(y, get_m_x(x), get_m_y(x)));
    fprintf('effort_m_x\n');
    test_grad(rand(numel(m_x), 1), ...
              @(y) effort    (get_rho(x), y, get_m_y(x)), ...
              @(y) effort_m_x(get_rho(x), y, get_m_y(x)));
    fprintf('effort_m_y\n');
    test_grad(rand(numel(m_y), 1), ...
              @(y) effort    (get_rho(x), get_m_x(x), y), ...
              @(y) effort_m_y(get_rho(x), get_m_x(x), y));
    fprintf('effort_x\n');
    test_grad(x, ...
              @(y) effort    (get_rho(y), get_m_x(y), get_m_y(y)), ...
              @(y) effort_x  (get_rho(y), get_m_x(y), get_m_y(y)));
    % lagrange
    fprintf('lagrange_rho\n');
    test_grad(rand(numel(rho), 1), ...
              @(y) lagrange    (y, get_m_x(x), get_m_y(x)), ...
              @(y) lagrange_rho(y, get_m_x(x), get_m_y(x)));
    fprintf('lagrange_m_x\n');
    test_grad(rand(numel(m_x), 1), ...
              @(y) lagrange    (get_rho(x), y, get_m_y(x)), ...
              @(y) lagrange_m_x(get_rho(x), y, get_m_y(x)));
    fprintf('lagrange_m_y\n');
    test_grad(rand(numel(m_y), 1), ...
              @(y) lagrange    (get_rho(x), get_m_x(x), y), ...
              @(y) lagrange_m_y(get_rho(x), get_m_x(x), y));
    fprintf('lagrange_x\n');
    test_grad(x, ...
              @(x) lagrange    (get_rho(x), get_m_x(x), get_m_y(x)), ...
              @(x) lagrange_x  (get_rho(x), get_m_x(x), get_m_y(x)));
    % penalty
    fprintf('penalty_rho\n');
    test_grad(rand(numel(rho), 1), ...
              @(y) penalty    (y, get_m_x(x), get_m_y(x)), ...
              @(y) penalty_rho(y, get_m_x(x), get_m_y(x)), 'd');
    fprintf('penalty_m_x\n');
    test_grad(rand(numel(m_x), 1), ...
              @(y) penalty    (get_rho(x), y, get_m_y(x)), ...
              @(y) penalty_m_x(get_rho(x), y, get_m_y(x)));
    fprintf('penalty_m_y\n');
    test_grad(rand(numel(m_y), 1), ...
              @(y) penalty    (get_rho(x), get_m_x(x), y), ...
              @(y) penalty_m_y(get_rho(x), get_m_x(x), y));
    fprintf('penalty_x\n');
    test_grad(x, ...
              @(x) penalty    (get_rho(x), get_m_x(x), get_m_y(x)), ...
              @(x) penalty_x  (get_rho(x), get_m_x(x), get_m_y(x)));
    % obj
    fprintf('obj_x\n');
    test_grad(x, ...
              @(x) obj  (get_rho(x), get_m_x(x), get_m_y(x)), ...
              @(x) obj_x(get_rho(x), get_m_x(x), get_m_y(x)), 'd');
  end
end
