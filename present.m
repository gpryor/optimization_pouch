% set up problem
clear all;
setup;
translating_point_present;
cvx_quiet true


% show boundary conditions
rho = gzeros(m_c);
rho(:,:,1)   = rho0;
rho(:,:,end) = rhoT;
gvolume(rho); 
fprintf('mass initial conditions\n');
pause;


% solve for mass preserving initial mapping
init_mu;
gvolume(reshape(rho, m_c));
fprintf('(mass preserving mapping) interpolated mass\n');
pause;
gvolume(reshape(m_x, m_f2));
fprintf('(mass preserving mapping) x velocity field\n');
pause;
gvolume(reshape(m_y, m_f3));
fprintf('(mass preserving mapping) y velocity field\n');
pause;


% solve for lagrange multipliers
fprintf('solving for lagrange multipliers\n');
init_q


t
