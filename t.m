% method parameters
R = 1;

% grid, stencils
N = 8;   % grid dims
m   = [N N N];
gen_metadata(m);
gen_stencils(m);

% grid meta data
n_front = m_c(1) * m_c(2); front  = 1:n_front;                      
n_back  = m_c(1) * m_c(2); back   = (1:n_back) + n_c - n_back;  

% input data
translating_point



rho = rand(n_f1, 1); m_x = rand(n_f2 ,1); m_y = rand(n_f3, 1);
true_effort = (2 * c_P_f1 * rho).^-1 .* (c_P_f2 * m_x.^2 + c_P_f3 * m_y.^2);


den = (2 * c_P_f1 * rho).^-1;

A = -1/2 * diag((c_P_f1 * rho).^-1) * diag(c_P_f2 * m_x.^2 + c_P_f3 * m_y.^2) * ...
    c_P_f1 * diag(rho.^-1);
B_x = diag((c_P_f1 * rho).^-1) * c_P_f2 * diag(m_x);
B_y = diag((c_P_f1 * rho).^-1) * c_P_f3 * diag(m_y);

% approx_effort = A * rho + B_x * m_x + B_y * m_y;
% plot(true_effort); hold on; plot(approx_effort, 'r'); hold off;

B_x = diag(c_P_f2 * m_x) * c_P_f2;
tmp1 = diag(c_P_f2 * m_x) * c_P_f2 * m_x;
tmp2 = B_x * m_x;
plot(tmp1); hold on; plot(tmp2, 'r'); hold off;
fcheck(tmp1, tmp2);



if (0)
% USE THIS IN THE NEXT SOLVER ATTEMPT
% we only fix the discretization problem when all quantities are on cell faces
% rho on faces; m on faces
% initial rho and m
cvx_begin
  variables rho(n_f1) m_x(n_f2) m_y(n_f3)
  minimize c_D2_f2 * m_x + c_D3_f3 * m_y    % smoothness
subject to
  c_D1_f1 * rho + c_D2_f2 * m_x + c_D3_f3 * m_y == 0;   % mass preservation
  rho(front) == rho0(:);
  rho(back) == rhoT(:);
cvx_end

BLAH = reshape(rho, [m_f1(1) m_f1(2) m_f1(3)]);
for i=1:16
  imagesc(BLAH(:,:,i));
  pause(0.1);
  drawnow;
end

% for i=1:16; subplot(4,4,i); imagesc(BLAH(:,:,i)); end
% drawnow;
end
