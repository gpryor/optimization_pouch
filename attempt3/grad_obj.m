function dL = grad_obj(x, s)
  metadata

  % constraints
  c7  = c_P_f1 * f1_D1_c * rho + c_D2_f2 * f2_P_c * m_x + c_D3_f3 * f3_P_c * m_y;
  c8a = [rho(front_slice) - s.rho0(:); zeros(n_c - numel(front_slice, 1), 1)];
  c8b = [zeros(n_c - numel(back_slice, 1), 1); rho(back_slice) - s.rhoT(:)];

  % grad constraints
  c7_rho = c_P_f1 * f1_D1_c;
  c7_m_x = c_D2_f2 * f2_P_c;
  c7_m_y = c_D3_f3 * f3_P_c;
  nf = numel(front_slice); nb = numel(back_slice); nr = numel(rho);
  c8a_rho = [      ones(nf, 1); zeros(nr - nf, 0)];
  c8b_rho = [zeros(nb - nr, 1);       ones(nb, 1)];


  % grad constraint checks
  test_grad(x, 
  

  
  % % grad by rho        (this is not correct)
  % effort_rho = s.a;
  % lagrange_rho = s.l_1 .* c7_rho + s.l_2 .* c8a_rho + s.l_3 .* c8b_rho;
  % penalty_rho = (1 ./ s.u_1) * c7_rho' * c7 + ...
  %               (1 ./ s.u_2) * c8a_rho' * c8a + ...
  %               (1 ./ s.u_3) * c8b_rho' * c8b;

  % % grad by m_x
  % effort_m_x = s.b_x;
  % lagrange_m_x = 0 * m_x;
  % lagrange_m_y = 0 * m_y;

  % % grad by m_y
  % effort_m_y = s.b_y;

  % dL = [effort_rho ; 

end



% % REFERENCE original lagrangian
% effort = dot(rho, s.a) + dot(m_x, s.b_x) + dot(m_y, s.b_y);
% lagrange = dot(s.l_1, c7) + dot(s.l_2, c8a) + dot(s.l_3, c8b);
% penalty = dot(1 ./ (2 * s.u_1),  c7 .^ 2) + ...
%           dot(1 ./ (2 * s.u_2), c8a .^ 2) + ...
%           dot(1 ./ (2 * s.u_3), c8b .^ 2);
% L = effort - lagrange + penalty;

