% meta data
gen_metadata(s.m);
gen_stencils(s.m);
slice_len = m_c(1) * m_c(2);
front_slice  = 1:slice_len;
back_slice   = (1:slice_len) + n_c - slice_len;

% unpack x
rho = x(      1:  n_c);
m_x = x(  n_c+1:2*n_c);
m_y = x(2*n_c+1:3*n_c);
% l_1 = x(3*n_c+1:4*n_c);
% tmp = 4*n_c+1;
% l_2 = x(            tmp:tmp + slice_len - 1);
% l_3 = x(tmp + slice_len:                end);
% mu = [rho; m_x; m_y];
% q = [s.a; s.b_x; s.b_y];
