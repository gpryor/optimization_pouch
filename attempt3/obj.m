% augmented lagrangian for OMT
function L = obj(x, s)
  metadata

  % constraints
  c7  = c_P_f1 * f1_D1_c * rho + c_D2_f2 * f2_P_c * m_x + c_D3_f3 * f3_P_c * m_y;
  c8a = [rho(front_slice) - s.rho0(:); zeros(n_c - numel(front_slice, 1), 1)];
  c8b = [zeros(n_c - numel(back_slice, 1), 1); rho(back_slice) - s.rhoT(:)];

  % augmented objective
  effort = dot(rho, s.a) + dot(m_x, s.b_x) + dot(m_y, s.b_y);
  lagrange = dot(s.l_1, c7) + dot(s.l_2, c8a) + dot(s.l_3, c8b);
  penalty = dot(1 ./ (2 * s.u_1),  c7 .^ 2) + ...
            dot(1 ./ (2 * s.u_2), c8a .^ 2) + ...
            dot(1 ./ (2 * s.u_3), c8b .^ 2);
  L = effort - lagrange + penalty;
end
