clear all;
setup;




% CVX can hit constraints with it's "subject to" facility
cvx_begin
  variables rho(n_c) m_x(n_c) m_y(n_c)
  minimize norm(m_x)
subject to
  c_P_f1 * f1_D1_c * rho + c_D2_f2 * f2_P_c * m_x + c_D3_f3 * f3_P_c * m_y == 0;
  rho(front_slice) == rho0(:);
  rho(back_slice) == rhoT(:);
cvx_end


% constraint 7
norm(c_P_f1 * f1_D1_c * rho + c_D2_f2 * f2_P_c * m_x + c_D3_f3 * f3_P_c * m_y)

% constraint 8a, 8b
norm(rho(front_slice) - rho0(:))
norm(rho(back_slice) - rhoT(:))





% can I hit constraints with an augmented lagrangian?
% it appears that I can. however, gradient descent is inferior to
% anything that SEDUMI is capable of. this is troubling given that gradient
% descent is the outer loop of our algorithm. 
cvx_begin
  variables rho(n_c) m_x(n_c) m_y(n_c)
  minimize norm(m_x) + ...
      norm(c_P_f1 * f1_D1_c * rho + c_D2_f2 * f2_P_c * m_x + c_D3_f3 * f3_P_c * m_y) + ...
      norm(rho(front_slice) - rho0(:)) + ...
      norm(rho( back_slice) - rhoT(:))
subject to
  % c_P_f1 * f1_D1_c * rho + c_D2_f2 * f2_P_c * m_x + c_D3_f3 * f3_P_c * m_y == 0;
  % rho(front_slice) == rho0(:);
  % rho(back_slice) == rhoT(:);
cvx_end


% constraint 7
norm(c_P_f1 * f1_D1_c * rho + c_D2_f2 * f2_P_c * m_x + c_D3_f3 * f3_P_c * m_y)

% constraint 8a, 8b
norm(rho(front_slice) - rho0(:))
norm(rho(back_slice) - rhoT(:))

