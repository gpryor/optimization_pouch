% gradient of energy wrt mx
function dLBYdmy = grad_my_cvx_test(m, psi, a, bx, by, rho, mx, my, rho0, rhoT, R)
  gen_metadata(m);
  gen_stencils(m);

  % change this!
  % dLBYdmy = c_P_f3' * (c_P_f3 * f3_D3_c * psi(:) - by(:));
  dLBYdmy = f3_D3_c * psi(:) - by(:);
end
