function A = Pfc_l_V( l, m )

  if l == 3      % not periodic in time dimension

    one = 1/2 * ones( m(l) + 1, 1 );
    A = spdiags( [ one one ], [ 0 1 ], m(l), m(l) + 1 );

  else

    one = 1/2 * ones( m(l) + 1, 1 );
    % * 0 for periodic grid, below
    A = spdiags( [ one one ], [ 0 1 ], m(l), m(l) + 1 * 0 );
    A(end, 1) = 1/2;   % for periodic grid

  end
end
