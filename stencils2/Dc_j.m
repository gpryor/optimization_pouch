% centers to faces
function acc = Dc_j( j, m )
  
  op = Dc_j_V( j, m );

  switch j
   case 1,
    acc = kron(speye(m(3)), speye(m(2)));
    acc = kron(        acc,          op);
   case 2,
    acc = kron(speye(m(3)),          op);
    acc = kron(        acc, speye(m(1)));
   case 3,
    acc = kron(         op, speye(m(2)));
    acc = kron(        acc, speye(m(1)));
  end

  % the following is incorrect: in reverse order
  if (0)
  if j == 1, acc = op; else acc = speye(m(1)); end;

  for i=2:length(m)
    if( i == j ) acc = kron(acc, op         );
    else         acc = kron(acc, speye(m(i)));
    end
  end
  end

end
