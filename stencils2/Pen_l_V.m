function A = Pen_l_V( l, m )
  f = 1/2 * ones( m(l) + 1, 1 ); f( 1 ) = 0;
  b = 1/2 * ones( m(l) + 1, 1 ); b( end - 1 ) = 0;
  A = spdiags( [ b f ], [ -1 0 ], m(l) + 1, m(l) );
end
