% stencil face->node projection
function A = Pnf_l_V( l, m )
  one = 1/2 * ones( m(l) + 1, 1 );
  A = spdiags( [ one one ], [ 0 1 ], m(l), m(l) + 1 );
end
