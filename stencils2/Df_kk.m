% operator: faces -> cells
function acc = Df_kk( k, m )
  
  op = Df_kk_V(k, m);

  switch k
   case 1,
    acc = kron(speye(m(3)), speye(m(2)));
    acc = kron(        acc,          op);
   case 2,
    acc = kron(speye(m(3)),          op);
    acc = kron(        acc, speye(m(1)));
   case 3,
    acc = kron(         op, speye(m(2)));
    acc = kron(        acc, speye(m(1)));
  end

  % erroneous; reversed
  if (0)
  if( k == 1 ) acc = op; else acc = speye( m(1) ); end;
  
  for i=2:length(m)
    if( i == k ) acc = kron( acc, op );
    else,        acc = kron( acc, speye( m(i) ) );
    end
  end
  end
  
end
