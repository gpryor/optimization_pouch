% operator: 2D cells -> arbitrary 2D grid  (n_c by n_c matrix)
% input is u is concatenated vector of 2 cell-centered grids:
%  [x loc. of sample points; y loc. of sample points]
function P = Pfu(u, m)

  % extract components of u
  n_c = length(u)/2;
  u_x = u(1:n_c); u_y = u(n_c+1:end);

  % compute x and y coordinates of involved elements
  TLx = floor(u_x); TLy = floor(u_y);
  TRx =  ceil(u_x); TRy = floor(u_y);
  BLx = floor(u_x); BLy =  ceil(u_y);
  BRx =  ceil(u_x); BRy =  ceil(u_y);

  % transform x, y coordinates to indices
  function I = xy2ind(X, Y), I = (X - 1) * m(1) + Y; end
  TLi = xy2ind(TLx, TLy);
  TRi = xy2ind(TRx, TRy);
  BLi = xy2ind(BLx, BLy);
  BRi = xy2ind(BRx, BRy);

  % construct values for non-zero indices for projection matrix
  H = frac(u_x); V = frac(u_y);
  mask = [TLi, TRi, BLi, BRi];
  TLv = (1 - H) .* (1 - V) ./ alpha(1);
  TRv = (    H) .* (1 - V) ./ alpha(2);
  BLv = (1 - H) .* (    V) ./ alpha(3);
  BRv = (    H) .* (    V) ./ alpha(4);

  % put final matrix together
  rows = repmat(1:n_c, [1, 4])';
  cols = [TLi; TRi; BLi; BRi];
  vals = [TLv; TRv; BLv; BRv];
  P = sparse(rows, cols, vals, n_c, n_c);

  % fractional part of number
  function b = frac(a), b = a - floor(a); end

  % duplicate normalization
  function b = alpha(a)
      tmp = {TLi, TRi, BLi, BRi};
      b = ones(size(tmp{a}));
      for i = [1:(a-1), (a+1):length(tmp)], b = b + (tmp{a} == tmp{i}); end
  end
end
