% operator: nodes -> cells
function acc = Pnc_l( m )

  acc = Pfc_l_V( 1, m );
  for i=2:length(m)
    acc = kron( Pfc_l_V( length(m), m ), acc );
  end

end
