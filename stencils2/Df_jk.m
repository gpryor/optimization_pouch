% operator: faces -> nodes
function acc = Df_jk( j, k, m )
  
  op = Df_jk_V( j, k, m );
  
  if( j == 1 ) acc = op; else acc = speye( m(1) + 1 ); end;
  
  for i=2:length(m)
    if( i == j ) acc = kron( acc, op ); else
      acc = kron( acc, speye( m(i) + 1 ) ); end
  end
  
end
