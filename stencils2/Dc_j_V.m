% centers to faces
function A = Dc_j_V( j, m )

  if j == 3     % not periodic in time dimension

    pos = ones( m(j) + 1, 1 ); pos( 1 ) = 0;
    neg = -ones( m(j) + 1, 1 ); neg( end - 1 ) = 0;
    A = spdiags( [ neg pos ], [ -1 0 ], m(j) + 1, m(j) );

  else

    pos = ones( m(j) + 1, 1 );
    neg = -ones( m(j) + 1, 1 );
    A = spdiags( [ neg pos ], [ -1 0 ], m(j) + 1 * 0, m(j) );
    A(  1, end) = -1;
    % A(end,   1) =  1;

  end
end
