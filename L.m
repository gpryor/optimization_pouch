% usage: new = L(q, reshape(psi_new, m_c), mu, rho0, rhoT, R)
function out = L(q, psi, mu, rho0, rhoT, R)
  assert(ndims(psi) > 2);

  gen_metadata(size(psi));     % defines m
  gen_stencils(m);

  grad_psi_q{1} = f1_D1_c * psi(:) - f1_P_c * q{1}(:);   % time
  grad_psi_q{2} = f2_D2_c * psi(:) - f2_P_c * q{2}(:);   % x
  grad_psi_q{3} = f3_D3_c * psi(:) - f3_P_c * q{3}(:);   % y

  dot_mu_grad_psi_q = sum(mu{1}(:) .* (c_P_f1 * grad_psi_q{1})) + ...
                      sum(mu{2}(:) .* (         grad_psi_q{2})) + ...
                      sum(mu{3}(:) .* (         grad_psi_q{3}));

  sum_grad_psi_q_squared = sum(sum(grad_psi_q{1} .* grad_psi_q{1})) + ...
                           sum(sum(grad_psi_q{2} .* grad_psi_q{2})) + ...
                           sum(sum(grad_psi_q{3} .* grad_psi_q{3}));

  % sum_grad_psi_q_squared = sum(sum_grad_psi_q_squared(:));
  % out = F(psi, q);

  out = F(psi, q) + G(psi, rho0, rhoT) + dot_mu_grad_psi_q + R/2 * sum_grad_psi_q_squared;
end

% q is on R x Rd, outputs R x Rd, therefore q = q{1}, q{2}, q{3}
function outside_K = F(psi, q)
  gen_metadata(size(psi));
  gen_stencils(m);

  C = q{1}(:) + (q{2}(:).^2 + q{3}(:).^2) / 2;
  if ~all(C < 1e-6),  outside_K = inf;  else,  outside_K = 0; end
end

% psi is on R x Rd, outputs to R; psi is simple scalar volume
function out = G(psi, rho0, rhoT)
  assert(ndims(psi) > 2);

  integrand = psi(:,:,1) .* rho0 - psi(:,:,end) .* rhoT;
  out = sum(integrand(:));
end
