% computes all stencils required for grid size m and sets in caller
% note that m = [size(T, 2) size(T, 1)];
% if str_flag exists, stencils are returned on a struct
function s = gen_stencils(m, str_flag)

  % persistence for caching
  persistent m_
  persistent f1_D1_c f2_D2_c f3_D3_c n_D2_f1 n_D1_f2 c_D1_f1 c_D2_f2 c_D3_f3 c_P_f1 c_P_f2 c_P_f3
  persistent f1_P_c f2_P_c f3_P_c c_P_n f1_P_n f2_P_n f3_P_n

  % fast path if repeated call
  if ~isempty(m_) && all(m_ == m)
    if exist('str_flag', 'var')
      s.f1_D1_c = f1_D1_c;
      s.f2_D2_c = f2_D2_c;
      s.f3_D3_c = f3_D3_c;
      s.c_D1_f1 = c_D1_f1;
      s.n_D2_f1 = n_D2_f1;
      s.n_D1_f2 = n_D1_f2;
      s.c_D2_f2 = c_D2_f2;
      s.c_D3_f3 = c_D3_f3;
      s.c_P_f1 = c_P_f1;
      s.c_P_f2 = c_P_f2;
      s.c_P_f3 = c_P_f3;
      s.f1_P_c = f1_P_c;
      s.f2_P_c = f2_P_c;
      s.f3_P_c = f3_P_c;
      s.c_P_n = c_P_n;
      s.f1_P_n = f1_P_n;
      s.f2_P_n = f2_P_n;
      s.f3_P_n = f3_P_n;
    else
      assignin('caller', 'f1_D1_c', f1_D1_c);
      assignin('caller', 'f2_D2_c', f2_D2_c);
      assignin('caller', 'f3_D3_c', f3_D3_c);
      assignin('caller', 'c_D1_f1', c_D1_f1);
      assignin('caller', 'n_D2_f1', n_D2_f1);
      assignin('caller', 'n_D1_f2', n_D1_f2);
      assignin('caller', 'c_D2_f2', c_D2_f2);
      assignin('caller', 'c_D3_f3', c_D3_f3);
      assignin('caller', 'c_P_f1',  c_P_f1 );
      assignin('caller', 'c_P_f2',  c_P_f2 );
      assignin('caller', 'c_P_f3',  c_P_f3 );
      assignin('caller', 'f1_P_c',  f1_P_c );
      assignin('caller', 'f2_P_c',  f2_P_c );
      assignin('caller', 'f3_P_c',  f3_P_c );
      assignin('caller', 'c_P_n' ,  c_P_n  );
      assignin('caller', 'f1_P_n',  f1_P_n );
      assignin('caller', 'f2_P_n',  f2_P_n );
      assignin('caller', 'f3_P_n',  f3_P_n );
    end
    return
  end
  m_ = m;
  
  % derivatives, operators (nodal operators not yet defined)
  f1_D1_c  = Dc_j( 1, m );
  f2_D2_c  = Dc_j( 2, m );
  f3_D3_c  = Dc_j( 3, m );
  c_D1_f1  = Df_kk(    1, m );
  c_D2_f2  = Df_kk(    2, m );
  c_D3_f3  = Df_kk(    3, m );
  n_D2_f1  = []; % Df_jk( 2, 1, m );
  n_D1_f2  = []; % Df_jk( 1, 2, m );
  c_P_f1   = Pfc_l(    1, m );
  c_P_f2   = Pfc_l(    2, m );
  c_P_f3   = Pfc_l(    3, m );
  f1_P_c   = c_P_f1';
  f2_P_c   = c_P_f2';
  f3_P_c   = c_P_f3';
  c_P_n    = []; % Pnc_l(       m );
  f1_P_n   = []; % Pnf_l(    1, m );
  f2_P_n   = []; % Pnf_l(    2, m );
  f3_P_n   = []; % Pnf_l(    3, m );

  % assign in caller
  if exist('str_flag', 'var')
    s.f1_D1_c = f1_D1_c;
    s.f2_D2_c = f2_D2_c;
    s.f3_D3_c = f3_D3_c;
    s.c_D1_f1 = c_D1_f1;
    s.n_D2_f1 = n_D2_f1;
    s.n_D1_f2 = n_D1_f2;
    s.c_D2_f2 = c_D2_f2;
    s.c_D3_f3 = c_D3_f3;
    s.c_P_f1 = c_P_f1;
    s.c_P_f2 = c_P_f2;
    s.c_P_f3 = c_P_f3;
    s.f1_P_c = f1_P_c;
    s.f2_P_c = f2_P_c;
    s.f3_P_c = f3_P_c;
    s.c_P_n = c_P_n;
    s.f1_P_n = f1_P_n;
    s.f2_P_n = f2_P_n;
    s.f3_P_n = f3_P_n;
  else
    assignin('caller', 'f1_D1_c', f1_D1_c);
    assignin('caller', 'f2_D2_c', f2_D2_c);
    assignin('caller', 'f3_D3_c', f3_D3_c);
    assignin('caller', 'c_D1_f1', c_D1_f1);
    assignin('caller', 'n_D2_f1', n_D2_f1);
    assignin('caller', 'n_D1_f2', n_D1_f2);
    assignin('caller', 'c_D2_f2', c_D2_f2);
    assignin('caller', 'c_D3_f3', c_D3_f3);
    assignin('caller', 'c_P_f1',  c_P_f1 );
    assignin('caller', 'c_P_f2',  c_P_f2 );
    assignin('caller', 'c_P_f3',  c_P_f3 );
    assignin('caller', 'f1_P_c',  f1_P_c );
    assignin('caller', 'f2_P_c',  f2_P_c );
    assignin('caller', 'f3_P_c',  f3_P_c );
    assignin('caller', 'c_P_n' ,  c_P_n  );
    assignin('caller', 'f1_P_n',  f1_P_n );
    assignin('caller', 'f2_P_n',  f2_P_n );
    assignin('caller', 'f3_P_n',  f3_P_n );
  end
end
