function A = Df_jk_V( j, k, m )
  pos = ones( m(j) + 1, 1 );  pos( 1 ) = 0;
  neg = -ones( m(j) + 1, 1 ); neg( end - 1 ) = 0;
  A = spdiags( [ neg pos ], [ -1 0 ], m(j) + 1, m(j) );
end
