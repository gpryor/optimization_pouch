function A = Df_kk_V( k, m )

  if k == 1      % not periodic in time dimension

    one = ones( m(k), 1 );
    A = spdiags( [ -one one ], [ 0 1 ], m(k), m(k) + 1);
    
  else

    one = ones( m(k), 1 );
    % * 0 for periodic grid, below
    A = spdiags( [ -one one ], [ 0 1 ], m(k), m(k) + 1 * 0 );
    A(end, 1) = 1;   % for periodic grid

  end
end
