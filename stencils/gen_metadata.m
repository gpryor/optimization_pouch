% if struct_flag is defined, metadata returned on struct s
function s = gen_metadata(T_or_m, struct_flag)
  
  % if we got T, set m.
  if numel(T_or_m) > 3,  m = [size(T_or_m, 1) size(T_or_m, 2) size(T_or_m, 3)];
  else                   m = T_or_m;                            end

  % gen metadata (modified to be periodic spatially)
  m_c   = m;                     % cell centered grid dims
  m_n   = m_c + 0;               % nodal grid dims
  m_f1  = m_c + [ 0 0 1 ];       % face grids
  m_f2  = m_c + [ 0 0 0 ];
  m_f3  = m_c + [ 0 0 0 ];
  n_c   = prod(m_c);
  n_n   = prod(m_n);
  n_f1  = prod(m_f1);
  n_f2  = prod(m_f2);
  n_f3  = prod(m_f3);

  % export
  if exist('struct_flag', 'var')
      s.m = m;
      s.m_c = m_c;
      s.m_n = m_n;
      s.m_f1 = m_f1;
      s.m_f2 = m_f2;
      s.m_f3 = m_f2;
      s.n_c = n_c;
      s.n_n = n_n;
      s.n_f1 = n_f1;
      s.n_f2 = n_f2;
      s.n_f3 = n_f3;
  else
      assignin('caller', 'm',    m);
      assignin('caller', 'm_c',  m_c);
      assignin('caller', 'm_n',  m_n);
      assignin('caller', 'm_f1', m_f1);
      assignin('caller', 'm_f2', m_f2);
      assignin('caller', 'm_f3', m_f3);
      assignin('caller', 'n_c',  n_c);
      assignin('caller', 'n_n',  n_n);
      assignin('caller', 'n_f1', n_f1);
      assignin('caller', 'n_f2', n_f2);
      assignin('caller', 'n_f3', n_f3);
  end

end
