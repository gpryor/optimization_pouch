% operator: faces -> cells (split)
function acc = Sfc_l( l, m, offset )

  op = Sfc_l_V( l, m, offset );

  if( l == 1 ) acc = op; else acc = speye( m(1) ); end;
  
  for i=2:length(m)
    if( i == l ) acc = kron( acc, op ); else
      acc = kron( acc, speye( m(i) ) ); end
  end
  
end
