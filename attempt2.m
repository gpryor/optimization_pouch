% grid, rho0, rhoT
setup
cvx_quiet false




% start out with a feasible, smooth mu
cvx_begin
  variables rho(n_c) m_x(n_f2) m_y(n_f3)
  minimize norm(c_P_f2 * f2_D2_c * m_x + c_P_f3 * f3_D3_c * m_y)
subject to
  c_P_f1 * f1_D1_c * rho + c_P_f2 * f2_D2_c * m_x + c_P_f3 * f3_D3_c * m_y == 0;
  rho(front_slice) == rho0(:);
  rho(back_slice) == rhoT(:);
cvx_end
x = [rho; m_x; m_y];


% show quality of our feasible starting point
L = 0;
E = sum((m_x.^2 + m_y.^2) ./ (2 * rho));
E_ = 0;
C7 = sum((c_P_f1 * f1_D1_c * rho + c_D2_f2 * f2_P_c * m_x + c_D3_f3 * f3_P_c * m_y).^2);
C8a = sum((rho(front_slice) - rho0(:)).^2);
C8b = sum((rho( back_slice) - rhoT(:)).^2);
fprintf('iter %04d  :', -1); fprintf('%f, ', [L, E_, E, C7, C8a, C8b]);
fprintf('\n');



iter = 0; converged = false;
while (~converged)

  % q; effort term as sup_q(convex function)
  lambda = -rho;
  b_x = -m_x ./ lambda;
  b_y = -m_y ./ lambda;
  a = -(b_x.^2 + b_y.^2) ./ 2;

  % unpack design variable
  rho = x(1:n_c);
  m_x = x(n_c+1:2*n_c);
  m_y = x(2*n_c+1:end);

  % iteration status
  L = attempt2_L(x, a, b_x, b_y, rho0, rhoT, m);
  E = sum((m_x.^2 + m_y.^2) ./ (2 * rho));
  E_ = sum(a .* rho + b_x .* m_x + b_y .* m_y);   % this part gets a little tricky
  C7 = sum((c_P_f1 * f1_D1_c * rho + c_D2_f2 * f2_P_c * m_x + c_D3_f3 * f3_P_c * m_y).^2);
  C8a = sum((rho(front_slice) - rho0(:)).^2);
  C8b = sum((rho( back_slice) - rhoT(:)).^2);
  fprintf('iter %04d  :', iter); fprintf('%f, ', [L, E_, E, C7, C8a, C8b]);
  fprintf('\n');
  iter = iter + 1;

  % gradient descent step to minimize wrt u
  L_x = attempt2_grad_L(x, a, b_x, b_y, rho0, rhoT, m);
  x = x - 0.001 * L_x;

  plot((m_x.^2 + m_y.^2) ./ (2 * rho)); drawnow;
end


