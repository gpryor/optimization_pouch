% simulated input data (rho0, rhoT)
rng_x = ((0:m_f3(2)-1) - m_f3(2)/2 + 0.5) / 10;
rng_y = ((0:m_f3(1)-1) - m_f3(1)/2 + 0.5) / 10;
[X, Y] = meshgrid(rng_x, rng_y);
rho0 = exp(-(X.^2 + Y.^2));


% normalize rho0 so that the minimium is about 0.14 and the max is 0.16
target_mn = 1; target_mx = 1.5;
mn = min(rho0(:)); mx = max(rho0(:));
rho0 = (rho0 - mn) / (mx - mn) * (target_mx - target_mn) + target_mn;

% translate distribution
c = 30;
rhoT = circshift(rho0, [c, 0]);
fcheck(sum(rho0(:)), sum(rhoT(:)));




% unused constraints on rho 0for testing against model problems
if (0)
% ensure \int x \rho = 0
TMP = X .* rho0 + Y .* rho0;
assert(sum(TMP(:)) < 1e-6);      % 1e-6 is decent precision

% ensure \int \|x\|^2 \rho = 1
T = (X .* X + Y .* Y) .* rho0;
rho0 = rho0 ./ sum(T(:));
TMP = (X .* X + Y .* Y) .* rho0;
fcheck(sum(TMP(:)), 1);
end
