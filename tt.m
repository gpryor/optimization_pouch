% % optimality contraint 2.1, 2.2 (v = grad of psi; our convex function)
% c2_1 = norm(mx ./ (f2_P_c * rho) - f2_D2_c * psi);
% c2_2 = norm(my ./ (f3_P_c * rho) - f3_D3_c * psi);


% subplot(2,1,1);
% T1_ = my ./ (f3_P_c * rho);
% T2_ = f3_D3_c * psi;
% plot(T1_); hold on; plot(T2_, 'r'); hold off;
% legend({'my and rho', 'dpsi/dy'}); title('baseline');
% drawnow;

% subplot(2,1,2);
% T1_ = (c_P_f3 * my) ./ rho;
% T2_ = c_P_f3 * f3_D3_c * psi;
% plot(T1_); hold on; plot(T2_, 'r'); hold off;
% legend({'my and rho', 'dpsi/dy'}); title('attempt 2');
% drawnow;

% return

% here, we were making sure that we had computed the correct gradient
% descent steps for the functional we had on hand
% gradients look good!
setup;
cvx_quiet true


% good place to test gradient
init_mu;     % checks out fully
init_q;      % appears to oscillate too much
a = q{1}; bx = q{2}; by = q{3};
rho = mu{1}; mx = mu{2}; my = mu{3};
psi = rand(n_c, 1);



L = energy(m, psi, a, bx, by, rho, mx, my, rho0, rhoT, R);
fprintf('initial %f\n', L);


% GRADIENT BY RHO
x0 = rho;
test_grad(x0, @(x)          energy(m, psi, a, bx, by, x, mx, my, rho0, rhoT, R), ...
              @(x) grad_rho_energy(m, psi, a, bx, by, x, mx, my, rho0, rhoT, R), 'd');

test_grad(x0, @(x)          cvx_test(m, psi, a, bx, by, x, mx, my, rho0, rhoT, R), ...
              @(x) grad_rho_cvx_test(m, psi, a, bx, by, x, mx, my, rho0, rhoT, R), 'd');


% GRADIENT BY MX
x0 = mx;
test_grad(x0, @(x)         energy(m, psi, a, bx, by, rho,  x, my, rho0, rhoT, R), ...
              @(x) grad_mx_energy(m, psi, a, bx, by, rho,  x, my, rho0, rhoT, R), 'd');

test_grad(x0, @(x)         cvx_test(m, psi, a, bx, by, rho,  x, my, rho0, rhoT, R), ...
              @(x) grad_mx_cvx_test(m, psi, a, bx, by, rho,  x, my, rho0, rhoT, R), 'd');




% GRADIENT BY MY
x0 = my;
test_grad(x0, @(x)         energy(m, psi, a, bx, by, rho, mx,  x, rho0, rhoT, R), ...
              @(x) grad_my_energy(m, psi, a, bx, by, rho, mx,  x, rho0, rhoT, R), 'd');

test_grad(x0, @(x)       cvx_test(m, psi, a, bx, by, rho, mx,  x, rho0, rhoT, R), ...
              @(x) grad_my_energy(m, psi, a, bx, by, rho, mx,  x, rho0, rhoT, R), 'd');

return
