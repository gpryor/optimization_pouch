% gradient of energy wrt rho
function dLBYdrho = grad_rho_cvx_test(m, psi, a, bx, by, rho, mx, my, rho0, rhoT, R)
  gen_metadata(m);
  gen_stencils(m);

  % change this!
  % dLBYdrho = c_P_f1 * (f1_D1_c * psi(:) - f1_P_c *  a(:));
  dLBYdrho = c_P_f1 * f1_D1_c * psi(:) - a(:);
end
