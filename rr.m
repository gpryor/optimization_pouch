% grid, stencils
N = 8;   % grid dims
m   = [N N N];
gen_metadata(m);
gen_stencils(m);

translating_point

%{
  given an M, we can get an X
  HOWEVER - - I don't think that we can reliably get nu from X once we have it
  
  Is this worth solving for? Well, given a nu, we need to be able to get ...
        an M. How else will we know that we solved for anything?
}%

return

% L0 is certainly not correct if it is not minimized by gold_m_x, gold_m_y
% with the given rho0 and rhoT
% what should phi be?
% L0(??phi <- don't have this, gold_m_x, gold_m_y, rho0, rhoT);
% I guess we should maximize according to phi?

init_mu;     % solve for rho

cvx_begin
  variables phi(n_c)
  maximize L0(phi, rho, gold_m_x(:), gold_m_y(:), rho0, rhoT, m_c);
cvx_end
