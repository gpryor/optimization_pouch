setup
cvx_quiet false


% random state
rho = rand(n_c, 1); m_x = rand(n_c, 1); m_y = rand(n_c, 1);


% original effort expression
L = (m_x.^2 + m_y.^2) ./ (2 * rho);

% solve for q given mu
% Simplified KKT conditions
%   1) rho + lambda = 0
%   2) m_x + lambda * b_x = 0
%   3) m_y + lambda * b_y = 0
% There is only one constraint and it must be active (lambda > 0)
% solving for maximizer via the KKT conditions
lambda = -rho;
b_x = -m_x ./ lambda;
b_y = -m_y ./ lambda;
a = -(b_x.^2 + b_y.^2) ./ 2;
L__ = a .* rho + b_x .* m_x + b_y .* m_y;


% checks (we are equivalent to ori. expression and active constrain holds)
active_constraint = a + (b_x.^2 + b_y.^2) ./ 2 <= 0;
fcheck(L, L__);
