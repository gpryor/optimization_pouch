clear all

% divergence testing

% grid
rng = 0:pi/10:pi;
N = length(rng);
[X, Y] = meshgrid(rng, rng);



% test function and its divergence
left = 1 : size(X,1);
right = (numel(X)-size(X,1) + 1) : numel(X);
top = 1:size(X,1):numel(X);
bottom = top + size(X,1) - 1;

F = exp(-(X - pi/2).^2 - (Y - pi/2).^2);
divF = -(2 * (X - pi/2) + 2 * (Y - pi/2)) .* F;

assert( all(F(left) == F(right)) );
assert( all(F(top) == F(bottom)) );

% subplot(1,2,1); imagesc(   F); title('exp(-(x - pi/2)^2)'); colorbar;
% subplot(1,2,2); imagesc(divF); title('div exp(-(x - pi/2)^2)'); colorbar;
% return


% stencils
m = [N N N];
gen_metadata(m);
gen_stencils(m);

% expand F, divF out through time
F    = repmat(   F, [1 1 N]); F    = F   (:);
divF = repmat(divF, [1 1 N]); divF = divF(:);

% divergence of a vector field
divF_ = c_D3_f3 * f3_P_c * F / (pi/10) + c_D2_f2 * f2_P_c * F / (pi/10);
% divF_ = c_P_f3 * f3_D3_c * F / (pi/10) + c_P_f2 * f2_D2_c * F / (pi/10);

clf;
plot(divF); hold on;
plot(divF_ , 'r'); hold off;
legend('gold', 'attempt');
title('this is how close we come on a cell centered grid to getting div correct');
