% grid, stencils
N = 8;   % grid dims
m   = [N N N];
gen_metadata(m);
gen_stencils(m);

% grid meta data
slice_len = m_c(1) * m_c(2);
front  = 1:slice_len;
back   = (1:slice_len) + n_c - slice_len;
n_front = numel(front);
n_back = numel(back);

% input data
translating_point
