addpath stencils
m = [3 3 3];
gen_metadata(m);
gen_stencils(m);


% save all stencils
f1_D1_c_ = f1_D1_c;
f2_D2_c_ = f2_D2_c;
f3_D3_c_ = f3_D3_c;
c_D1_f1_ = c_D1_f1;
c_D2_f2_ = c_D2_f2;
c_D3_f3_ = c_D3_f3;
c_P_f1_  = c_P_f1;
c_P_f2_  = c_P_f2;
c_P_f3_  = c_P_f3;


% compare with new stencils
addpath stencils2
gen_metadata(m);
gen_stencils(m);



% check
fcheck(f1_D1_c_, f3_D3_c);
fcheck(f2_D2_c_, f2_D2_c);
fcheck(f3_D3_c_, f1_D1_c);
fcheck(c_D1_f1_, c_D3_f3);
fcheck(c_D2_f2_, c_D2_f2);
fcheck(c_D3_f3_, c_D1_f1);
fcheck(c_P_f1_ , c_P_f3);
fcheck(c_P_f2_ , c_P_f2);
fcheck(c_P_f3_ , c_P_f1);


% make sure things are properly sized with non-cube grids
clear all
m = [8 8 4];
gen_metadata(m);
gen_stencils(m);

