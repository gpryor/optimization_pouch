function dL = attempt2_grad_L(x, a, b_x, b_y, rho0, rhoT, m)
  
  % meta data
  gen_metadata(m);
  gen_stencils(m);
  slice_len = m_c(1) * m_c(2);
  front_slice  = 1:slice_len;
  back_slice   = (1:slice_len) + n_c - slice_len;

  % unpack x
  rho = x(      1:  n_c);
  m_x = x(  n_c+1:2*n_c);
  m_y = x(2*n_c+1:3*n_c);
  % a   = x(3*n_c+1:4*n_c);
  % b_x = x(4*n_c+1:5*n_c);
  % b_y = x(5*n_c+1:  end);
 
  % grad
  tmp = (c_P_f1 * f1_D1_c * rho + c_D2_f2 * f2_P_c * m_x + c_D3_f3 * f3_P_c * m_y);
  drho = 2 * (c_P_f1 * f1_D1_c)' * tmp + a;
  dmx = 2 * (c_D2_f2 * f2_P_c)' * tmp + b_x;
  dmy = 2 * (c_D3_f3 * f3_P_c)' * tmp + b_y;

  drho(front_slice) = drho(front_slice) + 2 * (rho(front_slice) - rho0(:));
  drho( back_slice) = drho( back_slice) + 2 * (rho( back_slice) - rhoT(:));

  dL = [drho; dmx; dmy];

  % dL = [2 * rho; 0*m_x; 0*m_y];
end
