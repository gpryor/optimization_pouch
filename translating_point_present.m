% simulated input data (rho0, rhoT)
rng = ((0:N-1) - N/2 + 0.5) / 10 * 3;
[X, Y] = meshgrid(rng, rng);
rho0 = exp(-(X.^2 + Y.^2)) + 0.1;


% ensure \int x \rho = 0
TMP = X .* rho0 + Y .* rho0;
assert(sum(TMP(:)) < 1e-6);      % 1e-6 is decent precision



% ensure \int \|x\|^2 \rho = 1
T = (X .* X + Y .* Y) .* rho0;
rho0 = rho0 ./ sum(T(:));
TMP = (X .* X + Y .* Y) .* rho0;
fcheck(sum(TMP(:)), 1);



% translate distribution
c = 4;
rhoT = circshift(rho0, [c, 0]);
fcheck(sum(rho0(:)), sum(rhoT(:)));



% correct mapping according to paper (M is total mapping)
% defined on cell centers & sliceT to make grids easier
gold_M_x = zeros(m_c(2:3));
gold_M_y = zeros(m_c(2:3)) + c;



% compute infinitesimal mapping
% X(0,x) = x    % identity map   (first slice)
% d/dt X(t,x) = m(t, X(t,x))    % errrr. this is hard.
% M = X(T,x)    M is the final mapping


% identity map
[X, Y] = meshgrid(1:m_c(2), 1:m_c(3));
slice0 = 1:m_c(1)*m_c(2);
sliceT = n_c-m_c(1)*m_c(2)+1:n_c;
