function example
  % EXPLANATION:
  % The functions test_grad and test_jac compare the O(h) and O(h^2) Taylor
  % series errors using a given function f, and a gradient that you
  % provide, df/dx, at a point x0. We know a gradient is definitely correct
  % if the O(h^2) error is roughly the square of the O(h) error. Details on
  % how to use these functions are below.

  % EXAMPLE 1
  % example of how to test the gradient and jacobian of functions
  % NOTES:
  %   1. The gradient is DIFFERENT than the jacobian. The gradient 
  %      is df/dx whereas the Jacobian is df/dx'. This generalizes
  %      to matrices, as well. dF/dX is the gradient where dF/dX'
  %      is the Jacobian.
  %   2. test_grad can handle gradients of matrix functions whereas 
  %      test_jac can only handle Jacobians of vector functions.
  %   3. You should see a printout like the following when the
  %      gradient or Jacobian is correct:
  %      -------------------------------
  %      1.000000e-01 4.035272e-01 5.878312e-02
  %      1.000000e-02 4.083208e-02 5.889332e-04
  %      1.000000e-03 4.090531e-03 5.855386e-06
  %      1.000000e-04 4.091272e-04 5.851577e-08
  %      1.000000e-05 4.091346e-05 5.851191e-10
  %      1.000000e-06 4.091353e-06 5.851158e-12
  %      1.000000e-07 4.091354e-07 5.851763e-14
  %      1.000000e-08 4.091354e-08 8.152438e-16
  %      1.000000e-09 4.091354e-09 6.222512e-16
  %      1.000000e-10 4.091355e-10 5.396083e-16
  %      *** The exponent in the 3rd column should be roughly 2 times the
  %      exponent in the second column. If it isn't either two things are
  %      happening: (1) the gradient is written incorrectly or (2) the
  %      gradient is not-defined or incorrect at x0, the place you are
  %      testing the gradient, or due extra parameters you have picked for
  %      the base function (in this case eta). You'll see, for instance,
  %      that if eta is set to 0.01, the gradient will be poor. See EXAMPLE
  %      2, below, to sort this out.
  eta = 0.5;
  x0 = rand(100,1);
  test_grad(x0, @(x) sigmoid(x, eta), @(x) grad_sigmoid(x, eta));
  test_jac(x0, @(x) sigmoid(x, eta), @(x) jac_sigmoid(x, eta));


  % EXAMPLE 2
  % testing to see if you've made a mistake or the gradient does not exist
  % NOTES:
  %   1. Both test_grad and test_jac can optionally take in a fourth
  %      argument. If specified (it doesn't matter what it is), they will
  %      compute the gradient of f (sigmoid in this case) using your method
  %      and a finite difference approach.
  %   2. The output will look like the following,
  %      -------------------------------
  %      1.000000e-01 1.628123e+00 1.508817e+00 1.508813e+00
  %      1.000000e-02 6.154580e-02 1.917094e-02 1.916953e-02
  %      1.000000e-03 7.322451e-03 2.377719e-04 2.376246e-04
  %      1.000000e-04 7.499442e-04 2.440839e-06 2.426104e-06
  %      1.000000e-05 7.517863e-05 2.447365e-08 2.301662e-08
  %      1.000000e-06 7.519712e-06 2.448020e-10 1.337502e-10
  %      1.000000e-07 7.519897e-07 2.448065e-12 1.535291e-11
  %      1.000000e-08 7.519916e-08 2.447390e-14 1.716947e-12
  %      1.000000e-09 7.519918e-09 6.491116e-16 1.736504e-13
  %      1.000000e-10 7.519919e-10 6.990086e-16 1.743897e-14
  %      *** The fourth column is the output of the finite difference. We
  %      see that since the 3rd and 4th columns have exponents less than 2
  %      times the second column, both are poor estimates of the
  %      gradient. We can only assume that the gradient at x0 and eta is
  %      not well defined. Optimization procedures that depend on sigmoid
  %      at x0 and eta will not perform well. Note that the 3rd column is
  %      less than the 4th column. This means that we have implemented a
  %      gradient that is correct since it is equal to or better than the
  %      finite difference approach.
  %   3. GENERAL RULE - - if your gradient of a function is coming out
  %      WRONG, ALWAYS turn on 'd' to see if the gradient EXISTS AT ALL at
  %      x0 and parameters BEFORE PROCEEDING!!!
  eta = 0.01;
  x0 = rand(100,1);
  test_grad(x0, @(x) sigmoid(x, eta), @(x) grad_sigmoid(x, eta), 'd');

  % testing sigmoid at a more reasonable value of eta shows us a correct
  % user-provided and finite difference gradient
  eta = 0.6;
  x0 = rand(100,1);
  test_grad(x0, @(x) sigmoid(x, eta), @(x) grad_sigmoid(x, eta), 'd');





  % sigmoid function
  function x = sigmoid(r, eta)
      x = atan( r / eta ) / pi + 0.5;
      x = reshape(x, [length(r), 1]);   % output is column
  end

  % grad sigmoid function
  function x = grad_sigmoid(r, eta)
      x = (1 ./ ((r/eta).^2 + 1)) * (1/eta) * (1/pi);
    
      % grad is sparse column
      n = length(r);
      x = spdiags(x, 0, n, n);
      x = reshape(x, [n^2, 1]);
  end

  % jacobian of sigmoid function
  function x = jac_sigmoid(r, eta)
    x = (1 ./ ((r/eta).^2 + 1)) * (1/eta) * (1/pi);
  
    % jacobian is diagonal, sparse matrix
    n = length(r);
    x = spdiags(x, 0, n, n);
  end

end
