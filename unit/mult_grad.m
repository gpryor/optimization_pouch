%  A: p x q
%  X: s x t
%   dAdX
%        sp x tq
%     -----/\...-----
%     | pxq |     |
%     |     |     |
%     |--------------
%     \     |     |
%     /     |     |
%    ... ------------
%     |     |     |
%     |     |     |
%
function dAdX = mult_grad(dABYdX, dX, A)

  % meta data
  [p q] = size(A);
  [s t] = size_dX;

  % pull pieces of dABYdX out and place in dAdX
  dAdX = [];
  for i=0:(s-1)
    dAdX = [dAdX, dABYdX((1:p)+i*p, :)];
  end

  dAdX = reshape(dAdX, [p*q, s*t]);
  dX_ = dX';
  csdX_ = dX_(:);
  csdX_ = repmat(csdX_', [p*q 1]);
  dAdX = sum(dAdX .* csdX_, 2);

  % reshape for final result
  dAdX = reshape(dAdX, [p q]);

  function [s t] = size_dX
    tmp = size(dABYdX) ./ size(A);
    s = tmp(1); t = tmp(2);
  end

end
