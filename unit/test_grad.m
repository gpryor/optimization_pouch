% [f dfBYdx] must be functions where dfBYdx is the gradient of f
function test_grad(x, f, dfBYdx, diffjac_flag)
  % handle RNG
  persistent randstate;
  defaultStream = RandStream.getDefaultStream;
  if isempty(randstate), randstate = defaultStream.State; end;
  defaultStream.State = randstate;

  % gradient of matrix testing
  dx = randn(size(x));
  L = f(x);
  L_x = dfBYdx(x);
  L = double(L); L_x = double(L_x);
  if exist('diffjac_flag', 'var'), L_x_ = diffjac_grad(x); end;
  fprintf('-------------------------------\n');
  for j=1:10
    h = 10^(-j);
    Lf = f(x + h*dx);
    Lf = double(Lf);
    n1 = norm(Lf(:) - L(:));

    % compute more accurate gradient
    HOT = mult_grad(L_x, dx, L);
    n2 = norm(Lf(:) - L(:) - h * HOT(:));
    if exist('diffjac_flag', 'var')
        HOT_ = mult_grad(L_x_, dx, L);
        n3 = norm(Lf(:) - L(:) - h * HOT_(:));
        fprintf('%e %e %e %e\n', h, n1, n2, n3);
    else
        fprintf('%e %e %e\n', h, n1, n2);
    end

    % for the special case of relatively independent gradients, compute
    % accuracy on per-element basis
    

    % error out and stop if perf_data initially set to any character
    global perf_data;
    if ~isempty(perf_data)
      if ischar(perf_data), perf_data = []; end
      perf_data(end+1) = log10(n2) / log10(n1);

      % ideally, this should be simply perf_data(end) < 1.9, but there are problems with trig functions
      if (perf_data(end) < 1.7) && log10(n1) < -2 && log10(n2) > -9
        fprintf('%e [log10(%e)/log10(%e)]^-1 = %e\n', h, n1, n2, perf_data(end));
        perf_data = [];
        error('bad gradient accuracy');
      end
    end
  end

  function grad_f = diffjac_grad(x)
      grad_f = diffjac(x, f, f(x));
  end

end
