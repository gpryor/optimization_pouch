% test inputs
rmpath stencils
addpath stencils

% test data
n = 3;
T = randn(n);
R = randn(n);

% metadata
gen_metadata(T);
gen_stencils(m);

% stop if bad gradient
% global perf_data; perf_data = 'c';

% grad_T_norm_u test     WORKS
u = randn(n_f1 + n_f2, 1);
test_grad(u, @(x) T_norm_u(T, x, m), @(x) grad_T_norm_u(T, x, m), 'd');

return


% HS omt energy test
% we give a "fair" input, here. gradient of smoothing and matching terms
% are incorrect in certain situations. don't know how to fix. this will
% certainly fail if n = 3.
[u0_x u0_y] = meshgrid(1:m_c(2), 1:m_c(1));
u0(:,:,1) = u0_x + 2; u0(:,:,2) = u0_y + 2;
map_u = 0 * [f1_P_c * u0_x(:); f2_P_c * u0_y(:)] + rand(n_f1 + n_f2, 1);
map_u(round(map_u) == map_u) = map_u(round(map_u) == map_u) + 0.01;
test_grad(map_u, ...
          @(x) HS_omt_lin(x, T, R, 0), ...
          @(x) grad_HS_omt_lin(x, T, R, 0));

return


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INCOMPLETE - - NEED TO FINISH THIS LATER; OCCLUSION DETECTION
% map_reverse function testing
% note that map relies on interp2(...,'linear',0), which means that the
% area we choose to test around (x0) needs to avoid the following
%  - discontinuities in map (i.e. at whole numbers)
%  - boundaries on input signal
T = T * 0 + reshape(1:numel(T), size(T));
[X, Y] = meshgrid(1:n, 1:n);
map_x0 = [X(:); Y(:)];
test_grad(map_x0, @(x) map_reverse(x, T, m_c), ...
          @(x) grad_map_reverse(x, T, m_c));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% test_jac(map_x0, @(x) map(x, T, m_c), @(x) jac_map(x, T, m_c));



% HS sym energy test
% we give a "fair" input, here. gradient of smoothing and matching terms
% are incorrect in certain situations. don't know how to fix. this will
% certainly fail if n = 3.
[u0_x u0_y] = meshgrid(1:m_c(2), 1:m_c(1));
u0(:,:,1) = u0_x + 2; u0(:,:,2) = u0_y + 2;
map_u = 0 * [f1_P_c * u0_x(:); f2_P_c * u0_y(:)] + rand(n_f1 + n_f2, 1);
map_u(round(map_u) == map_u) = map_u(round(map_u) == map_u) + 0.01;
test_grad(map_u, @(x) HS_sym(x, u0, T, R, 0), @(x) grad_HS_sym(x, u0, T, R, 0),'d');

% right_square
eta = 0.5; c1 = -1; c2 = 0;
u = randn(n_f1 + n_f2, 1) * 3;
test_grad(u, @right_square, @grad_right_square);
test_jac(u, @right_square, @jac_right_square);

% elastic field penalty
eta = 1;
u = randn(n_f1 + n_f2, 1);
test_jac(u, @(x) elastic(x, m), @(x) jac_elastic(x, m));
test_grad(u, @(x) elastic(x, m), @(x) grad_elastic(x, m));


% HS_elastic energy test
% we give a "fair" input, here. gradient of smoothing and matching terms
% are incorrect in certain situations. don't know how to fix. this will
% certainly fail if n = 3
[X Y] = meshgrid(1:m_c(2), 1:m_c(1));
map_u = [f1_P_c * X(:); f2_P_c * Y(:)] + rand(n_f1 + n_f2, 1);
map_u(map_u<2) = 2; map_u(map_u>n-1) = n-1; 
map_u(round(map_u) == map_u) = map_u(round(map_u) == map_u) + 0.01;
test_grad(map_u, @(x) HS_elastic(x, T, R, 1), @(x) grad_HS_elastic(x, T, R, 1));

% heaviside approximation
eta = 0.5; c1 = -1; c2 = 0;
u = randn(n_f1 + n_f2, 1) * 3;
test_jac(u, @(x) heaviside(x, eta, c1, c2), ...
         @(x) jac_heaviside(x, eta, c1, c2));
test_grad(u, @(x) heaviside(x, eta, c1, c2), ...
          @(x) grad_heaviside(x, eta, c1, c2));

% elastic field penalty
eta = 1;
u = randn(n_f1 + n_f2, 1);
test_jac(u, @(x) elastic(x, m), @(x) jac_elastic(x, m));
test_grad(u, @(x) elastic(x, m), @(x) grad_elastic(x, m));

% sigmoid gradient and jacobian works
% sigmoid has inaccurate gradient away from origin. as eta decreases, this
% effect is more pronounced. eta=0.5 and testing on [0,1] seems to be good.
eta = 0.5;
x0 = rand(100,1);
test_grad(x0, @(x) sigmoid(x, eta), @(x) grad_sigmoid(x, eta));
test_jac(x0, @(x) sigmoid(x, eta), @(x) jac_sigmoid(x, eta));

% HS_mag energy test
% we give a "fair" input, here. gradient of smoothing and matching terms
% are incorrect in certain situations. don't know how to fix. this will
% certainly fail if n = 3
[X Y] = meshgrid(1:m_c(2), 1:m_c(1));
map_u = [f1_P_c * X(:); f2_P_c * Y(:)] + rand(n_f1 + n_f2, 1);
map_u(map_u<2) = 2; map_u(map_u>n-1) = n-1; 
map_u(round(map_u) == map_u) = map_u(round(map_u) == map_u) + 0.01;
test_grad(map_u, @(x) HS_mag(x, T, R, 1), @(x) grad_HS_mag(x, T, R, 1));

% HS energy test
% we give a "fair" input, here. gradient of smoothing and matching terms
% are incorrect in certain situations. don't know how to fix. this will
% certainly fail if n = 3.
[X Y] = meshgrid(1:m_c(2), 1:m_c(1));
map_u = [f1_P_c * X(:); f2_P_c * Y(:)] + rand(n_f1 + n_f2, 1);
map_u(map_u<2) = 2; map_u(map_u>n-1) = n-1; 
map_u(round(map_u) == map_u) = map_u(round(map_u) == map_u) + 0.01;
test_grad(map_u, @(x) HS(x, T, R, 1), @(x) grad_HS(x, T, R, 1));

% match function testing
% we must take the same care, here, as we do with the map functions 
[X Y] = meshgrid(1:m_c(2), 1:m_c(1));
map_u = [f1_P_c * X(:); f2_P_c * Y(:)] + rand(n_f1 + n_f2, 1);
map_u(map_u<2) = 2; map_u(map_u>n-1) = n-1; 
map_u(round(map_u) == map_u) = map_u(round(map_u) == map_u) + 0.01;
test_grad(map_u, @(x) L2_match_lin(x, T, R), @(x) grad_L2_match_lin(x, T, R));
test_grad(map_u, @(x) L2_match(x, T, R), @(x) grad_L2_match(x, T, R));

% map function testing
% note that map relies on interp2(...,'linear',0), which means that the
% area we choose to test around (x0) needs to avoid the following
%  - discontinuities in map (i.e. at whole numbers)
%  - boundaries on input signal
map_x0 = randn(2*n_c, 1) * 0.001 + 1.5;
test_grad(map_x0, @(x) map(x, T, m_c), @(x) grad_map(x, T, m_c));
test_jac(map_x0, @(x) map(x, T, m_c), @(x) jac_map(x, T, m_c));

% test all components' gradients
u = randn(n_f1 + n_f2, 1);
test_grad(u, @norm_u, @grad_norm_u);
test_grad(u, @(x) norm_grad_u(x, m), @(x) grad_norm_grad_u(x, m));
test_grad(u, @(x) L2_match(x, T, R), @(x) grad_L2_match(x, T, R));
test_grad(u, @(x) L2_match_lin(x, T, R), @(x) grad_L2_match_lin(x, T, R));
