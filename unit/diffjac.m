function [l, u] =diffjac(x, f, f0)
% compute a forward difference Jacobian f'(x), return lu factors
%
% uses dirder.m to compute the columns
%
% C. T. Kelley, November 25, 1993
%
% This code comes with no guarantee or warranty of any kind.
%
%
% inputs:
%         x, f = point and function
%		  f0   = f(x), preevaluated
%


% if f0 is vector, do standard version, else do matrix version
if isvector(f0)
  n=length(x);
  for j=1:n
    zz=zeros(n,1);
    zz(j)=1;
    jac(:,j)=dirder(x,zz,f,f0);

    % fprintf('%d/%d\n', j, n);

  end
else
  
  m = size(f0);
  vec_f0 = f0(:);

  tmp = diffjac(x, @vec_f, vec_f0);
  jac = reshape(tmp, [size(tmp,1)/m(2), size(tmp,2)*m(2)]);

end

function x_out = vec_f(x)
  x_out = f(x);
  x_out = x_out(:);
end


% do lu if requested
if (nargout == 2) [l, u] = lu(jac);
else              l      = jac;      end;


end
