% [f dfBYdx_] must be functions where dfBYdx is the jacobian of f
function test_jac(x, f, dfBYdx_, diffjac_flag)
  % sanity check to avoid getting inputs we can't test
  f_of_x = f(x);
  if ~(isscalar(f_of_x) || isvector(f_of_x)) error; end;

  % jacobian of matrix testing
  dx = randn(size(x));
  L = f(x);
  L_x = dfBYdx_(x);
  if exist('diffjac_flag', 'var')
      L_x_ = diffjac(x, f, f(x)); 
  end
  fprintf('-------------------------------\n');
  L = double(L); L_x = double(L_x);
  for j=1:10
    h = 10^(-j);
    Lf = f(x + h*dx);
    Lf = double(Lf);
    n1 = norm(Lf(:) - L(:));

    % compute more accurate function approximation
    HOT = L_x * dx;
    n2 = norm(Lf(:) - L(:) - h * HOT(:));
    if exist('diffjac_flag', 'var')
        HOT_ = L_x_ * dx;
        n3 = norm(Lf(:) - L(:) - h * HOT_(:));
        fprintf('%e %e %e %e\n', h, n1, n2, n3);
    else
        fprintf('%e %e %e\n', h, n1, n2);
    end

    % error out and stop if perf_data initially set to any character
    global perf_data;
    if ~isempty(perf_data)
      if ischar(perf_data), perf_data = []; end
      perf_data(end+1) = log10(n2) / log10(n1);

      % ideally, this should be simply perf_data(end) < 1.9, but there are problems with trig functions
      if (perf_data(end) < 1.5) && log10(n1) < -2 && log10(n2) > -9
        fprintf('%e [log10(%e)/log10(%e)]^-1 = %e\n', h, n1, n2, perf_data(end));
        perf_data = [];
        error('bad gradient accuracy');
      end
    end

  end

end
