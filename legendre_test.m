setup


% random state
rho = rand(n_c, 1); mx = rand(n_c, 1); my = rand(n_c, 1);


% original effort expression
L = (mx.^2 + my.^2) ./ (2 * rho);


% legendre transform equivalent
cvx_begin
  variables a(n_c) bx(n_c) by(n_c)
  maximize sum(a .* rho + bx .* mx + by .* my)
subject to
  a + (bx .^ 2 + by .^ 2) / 2 <= 0;
cvx_end
L_ = a .* rho + bx .* mx + by .* my;


% original effort expression should match legendre
fcheck(L, L_);


% optionally plot
% plot(L); hold on;
% plot(L_, 'r'); hold off;
