% clear all;
setup;
addpath unit;

if ~exist('rho', 'var')
  % random start point
  rho = rand(n_c, 1); m_x = rand(n_c, 1); m_y = rand(n_c, 1);
  x = [rho; m_x; m_y];
end


% % start out with a feasible smooth mu
% cvx_begin
%   variables rho(n_c) m_x(n_f2) m_y(n_f3)
%   minimize norm(c_P_f2 * f2_D2_c * m_x + c_P_f3 * f3_D3_c * m_y)
% subject to
%   c_P_f1 * f1_D1_c * rho + c_P_f2 * f2_D2_c * m_x + c_P_f3 * f3_D3_c * m_y == 0;
%   rho(front_slice) == rho0(:);
%   rho(back_slice) == rhoT(:);
% cvx_end
% C7 = norm(c_P_f1 * f1_D1_c * rho + c_D2_f2 * f2_P_c * m_x + c_D3_f3 * f3_P_c * m_y)
% C8a = norm(rho(front_slice) - rho0(:))
% C8b = norm(rho( back_slice) - rhoT(:))
% x = [rho; m_x; m_y];


converged = false;
while (~converged)

  L = gd_test_L(x, rho0, rhoT, m);
  % [L, C7, C8a, C8b]

  % L_x = diffjac(x, ...
  %               @(y) gd_test_L(y, rho0, rhoT, m), ...
  %               gd_test_L(x, rho0, rhoT, m));

  L_x = gd_test_grad_L(x, rho0, rhoT, m);
  x = x - 0.1 * L_x;

  % unpack x to see constraints
  rho = x(1:n_c);
  m_x = x(n_c+1:2*n_c);
  m_y = x(2*n_c+1:end);

  C7 = norm(c_P_f1 * f1_D1_c * rho + ...
            c_D2_f2 * f2_P_c * m_x + ...
            c_D3_f3 * f3_P_c * m_y);
  C8a = norm(rho(front_slice) - rho0(:));
  C8b = norm(rho( back_slice) - rhoT(:));

  [L, C7, C8a, C8b]

end

