% laplace with boundary conditions. will work this into multigrid for a
% good "initial guess for OMT once grids are sorted out"

% method parameters
R = 1;

% grid, stencils
N = 16;   % grid dims
m   = [N N N];
gen_metadata(m);
gen_stencils(m);

% grid meta data
n_front = m_c(1) * m_c(2); front  = 1:n_front;                      
n_back  = m_c(1) * m_c(2); back   = (1:n_back) + n_c - n_back;  

% input data
translating_point




% this is the actual system we want to solve (actually fully determined)
A_f = [speye(n_front), sparse(n_front, n_c - n_front)];
A_b = [sparse(n_back, n_c - n_back), speye(n_back)];
A_d2 = f1_D1_c' * f1_D1_c + f2_D2_c' * f2_D2_c + f3_D3_c' * f3_D3_c;


% A = [A_f ; A_d2 ; A_b];

A = A_d2;
A(front,:) = A(front,:) + A_f;
A( back,:) = A( back,:) + A_b;

b = [rho0(:); zeros(n_c - n_front - n_back, 1); rhoT(:)];


x = A \ b;
