% simulated input data (rho0, rhoT)
rng = ((0:N-1) - N/2 + 0.5) / 4 * 4;
[X, Y] = meshgrid(rng, rng);
Y = Y * 0;
rho0 = exp(-(X.^2 + Y.^2)) + 0.1;
rhoT = circshift(rho0, [0, 1] + 0 * [N/2, 2]);
fcheck(sum(rho0(:)), sum(rhoT(:)));
