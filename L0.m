% implements original energy on page 384 of benamous
function out = L0(psi, rho, m_x, m_y, rho0, rhoT, m_c)
  gen_metadata(m_c);     % defines m
  gen_stencils(m);

  % terms in first integrand
  T1 = (c_P_f2 * (m_x .^ 2) + c_P_f3 * (m_y .^ 2)) ./ (2 * rho);
  T2 = (c_P_f1 * f1_D1_c * psi) .* rho;
  T3 = (f2_D2_c * psi) .* m_x  + (f3_D3_c * psi) .* m_y;

  % evaluate first integrand
  I1 = sum(T1 - T2 - T3);

  % terms in second integrand
  slice0 = 1:numel(rho0);
  sliceT = (numel(psi) - numel(rhoT)):numel(psi);
  T1 = psi(slice0) .* rho0;
  T2 = psi(sliceT) .* rhoT;

  % evaluate second integrand
  I2 = sum(T1 - T2);

  out = I1 - I2;
end
