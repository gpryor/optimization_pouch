% sigmoid function
function x = sigmoid(r, eta)
  x = atan( r / eta ) / pi + 0.5;
  x = reshape(x, [length(r), 1]);   % output is column
end
