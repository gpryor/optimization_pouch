function fcheck(A, B, varargin)
% true if every element is equal (forces reduction if GPU)
  tol = 1e-5;
  is_rel = true;
  
  % parse optional parmeters
  for i = 1:numel(varargin)
    arg = varargin{i};
    if ischar(arg)
      switch arg
       case 'abs', is_rel = false;
       case 'rel', is_rel = true;
       otherwise error('unknown parameter');
      end
    elseif isscalar(arg)
      tol = arg;
    else
      error('unknown paramter');
    end
  end

  if ~isequal(size(A), size(B))
    disp('size mismatch')
    error(mfilename);
  end
  
  a = flat(double(A));
  b = flat(double(B));
  
  err = abs(a - b);
  [diff ind] = max(flat(err));
  if is_rel
    diff = norm(a - b) / (norm(a) + eps);
  end
  if diff > tol || isnan(diff)
    A_ = iff(isempty(inputname(1)), 'tmp', inputname(1));
    B_ = iff(isempty(inputname(2)), 'tmp', inputname(2));
    error('%s(%d) = %ld\n%s(%d) = %ld\n(diff = %ld    tol = %ld)\n', ...
          A_, ind, a(ind), B_, ind, b(ind), diff, tol);
  end
end


function m = flat(m)
  m = m(:);
end


function F = iff(test, T, F)
% similar to ternary operator in C   out = cond ? T : F
  if test, F = T; end
end
