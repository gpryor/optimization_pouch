function gradmag_solver
  % settings
  alpha = 4;
  c = [-0.3; 0.1];

  % start from random point
  x = rand(2,1);

  % % checks
  % % check objective, gradient
  % test_grad(x, @obj, @grad_obj);
  % % check constraint, gradient
  % test_grad(x, @constraint, @grad_constraint);
  % % check aug. lag.,  gradient
  % test_grad(x, @aug_lagrangian, @grad_aug_lagrangian);

  % % check lagrange multiplier obj,  gradient
  % % (can't use x because the following also requires a lambda)
  % test_grad(rand(3,1), @obj_w_lagrange, @grad_obj_w_lagrange);
  % % check lagrange multiplier obj, hessian
  % test_grad(rand(3,1), @grad_obj_w_lagrange, @grad2_obj_w_lagrange);
  % % check mag grad and grad mag grad
  % test_grad(rand(3,1), @mag_grad_obj_w_lagrange, @grad_mag_grad_obj_w_lagrange);




  % % EXHIBIT 1
  % % gradient descent on objective (does what it is supposed to do)
  % iter = 0; converged = false;
  % while (~converged)
  %   L = obj(x);
  %   L_x = grad_obj(x);
  %   x = x - 0.001 * L_x;

  %   fprintf('x:(%f,%f)    L: %f   obj: %f   const. %f\n', ...
  %           x(1), x(2), L, obj(x), constraint(x));
  % end




  % % EXHIBIT 2
  % % gradient descent on augmented lagrangian
  % % this fails to hit the constraint and how far away from the constraint
  % % we land depends on the scaling of the objective wrt to the constraint
  % % terms. we seek to solve this problem in EXHIBIT 3, immediately below
  % h = inline('(x + -0.3)^2 + (y + 0.1)^2 - 4', 'x', 'y');
  % iter = 0; converged = false;
  % while (~converged)
  %   L = aug_lagrangian(x);
  %   L_x = grad_aug_lagrangian(x);
  %   x = x - 0.01 * L_x;

  %   plot(x(1), x(2), 'rx', 'MarkerSize', 10, 'Linewidth', 3); hold on;
  %   implot2(h, 'LineWidth', 3);
  %   plot(    0,     0, 'bo', 'MarkerSize', 10, 'Linewidth', 3);
  %   plot(-c(1), -c(2), 'gx', 'MarkerSize', 10, 'Linewidth', 3); hold off;
  %   drawnow;
  % end





  % EXHIBIT 3
  % attempt at hitting the constraint by minimizing magnitude of gradient
  x = rand(3, 1) * 2;    % there is a lambda now in the mix
  h = inline('(x + -0.3)^2 + (y + 0.1)^2 - 4', 'x', 'y');
  iter = 0; converged = false;
  while (~converged)
    L = mag_grad_obj_w_lagrange(x);
    L_x = grad_mag_grad_obj_w_lagrange(x);
    x = x - 0.01 * L_x;

    fprintf('x:(%f,%f)    L: %f   obj: %f   const. %f\n', ...
            x(1), x(2), L, obj(x(1:2)), constraint(x(1:2)));

    plot(x(1), x(2), 'rx', 'MarkerSize', 10, 'Linewidth', 3); hold on;
    implot2(h, 'LineWidth', 3);
    plot(-c(1), -c(2), 'gx', 'MarkerSize', 10, 'Linewidth', 3);
    plot(    0,     0, 'bo', 'MarkerSize', 10, 'Linewidth', 3); hold off;
    axis equal;
    drawnow;
  end



  % magnitude of gradient of lagrangian w/ lagrange multipliers
  function L = obj_w_lagrange(x)
    % lambda is x(3)
    L = obj(x(1:2)) - x(3) * constraint(x(1:2));
  end
  function dL = grad_obj_w_lagrange(x)
    % lambda is x(3)
    dLBYdx = grad_obj(x(1:2)) - x(3) * grad_constraint(x(1:2));
    dLBYdlambda = -constraint(x(1:2));
    dL = [dLBYdx; dLBYdlambda];
  end
  function dL = grad2_obj_w_lagrange(x)
    gc = grad_constraint(x(1:2));
    dL = [alpha * 2 + -2 * x(3), 0                    , -gc(1);
          0                    , -2 * x(3) + alpha * 2, -gc(2);
          -gc(1)               , -gc(2),                    0];
  end
  function L = mag_grad_obj_w_lagrange(x)
    L = sum(grad_obj_w_lagrange(x).^2);
  end
  function dL = grad_mag_grad_obj_w_lagrange(x)
    dL = 2 * grad2_obj_w_lagrange(x) * grad_obj_w_lagrange(x);
  end



  % minimize using gradient descent on augmented lagrangian
  function L = aug_lagrangian(x)
    L = obj(x) + constraint(x)^2;
  end
  function dL = grad_aug_lagrangian(x)
    dL = grad_obj(x) + 2 * grad_constraint(x) * constraint(x);
  end



  % base functions (objective and constraint)
  function L = obj(x)
    L = alpha * sum(x .^ 2);
  end

  function dL = grad_obj(x)
    dL = alpha * 2 * x;
  end

  function C = constraint(x)
    x = (x + c);
    C = x' * x - 4;
  end

  function dC = grad_constraint(x)
    dC = 2 * x + 2 * c;
  end
end
