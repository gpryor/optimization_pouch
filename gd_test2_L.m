function L = gd_test2_L(x, rho0, rhoT, m)
  
  % meta data
  gen_metadata(m);
  gen_stencils(m);
  slice_len = m_c(1) * m_c(2);
  front_slice  = 1:slice_len;
  back_slice   = (1:slice_len) + n_c - slice_len;

  % unpack x
  rho = x(1:n_c);
  m_x = x(n_c+1:2*n_c);
  m_y = x(2*n_c+1:3*n_c);
  psi1 = x(3*n_c+1:4*n_c);
  tmp = 4*n_c+1;
  psi2 = x(tmp:tmp+length(front_slice)-1);
  psi3 = x(tmp+length(front_slice):end);

  % energy
  L = -norm(m_x) - 0 * sum(psi2 .* (rho(front_slice) - rho0(:)));

 % - ...
 %      sum(psi1 .* (c_P_f1 * f1_D1_c * rho + ...
 %                   c_D2_f2 * f2_P_c * m_x + ...
 %                   c_D3_f3 * f3_P_c * m_y)) - ...
 %      sum(psi2 .* (rho(front_slice) - rho0(:))) - ...
 %      sum(psi3 .* (rho( back_slice) - rhoT(:)));
end
